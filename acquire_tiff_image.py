#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun 20 16:23:00 2020

@brief: Take a single image as TIFF.
        Upload detconf_imgtiff and detacq_imgtiff scripts,
        Configure and run the detector,
        download resulting TIFF images.

@author: Till, till.dreier@med.lu.se
"""

from pylambda.lmbd.lmbdssh import LambdaRemote
import os

if __name__ == "__main__":

    L = LambdaRemote("192.168.253.17", "xspadmin", "l3tm3w0rk")

    # %% config

    # settings
    exposure_time_ms = 1000
    threshold_eV = 6000
    savename = "align"

    # download folder
    save_path = "/data2/xraylab/measurements/lambda350k"

    # remote folder
    remote_folder = "/home/xspadmin/tmp/" + L.__get_date__() + "_" + savename

    # scripts to run on detector
    config_script = "detconf_imgtiff.py"
    acq_script = "detacq_imgtiff.py"
    local_script_path = "pylambda/remote"  # FIXME: the absolute path could be required (depends on editor)
    remote_script_path = "/home/xspadmin/tmp/testscripts"

    # %% upload scripts

    L.upload_files(local_script_path, remote_script_path, [config_script, acq_script])

    # %% config detector

    argv_config = str(exposure_time_ms) + " " + str(threshold_eV)
    L.execute_script(remote_script_path, config_script, argv_config)

    # %% take image

    argv_acquire = remote_folder + " " + str(exposure_time_ms)

    # ---- update the energy threshold ------------------------------------------------------------------------
    # new_energy_thl = 12000
    # argv_acquire = remote_folder + " " + str(exposure_time_ms) + " " + str(new_energy_thl)
    # ---------------------------------------------------------------------------------------------------------

    L.execute_script(remote_script_path, acq_script, argv_acquire)

    # %% download all images from folder

    # make folder
    L.make_download_folder(save_path, L.__get_date__() + "_" + savename, no_unique_name=True)
    download_folder = L.downloadfolder

    # download files
    L.download_from_directory(remote_folder, os.path.join(save_path, download_folder), ".tiff")
