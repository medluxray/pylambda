#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 18 12:35:00 2020

@brief: Acquire multiple images on the detector using a "software trigger".
        Images are written into a custom HDF5 file (master + N data files with M images per file).

@author: Till, till.dreier@med.lu.se
"""

from pylambda.lmbd.lmbdssh import LambdaRemote
import time
import os


def extract_created_folder(script_out):
    """ Find the name of the created measurement folder in the output from the detacq_ownhdf5.py script. """
    remotefolder = None
    for output in script_out:
        if "FOLDER: " in output:
            remotefolder = output.split(": ")[-1].strip("\n").split("/")[-1]

    # check if folder was found and set correctly
    if remotefolder is not None:
        print('[INFO:] folder on remote host:  {}'.format(remotefolder))
    else:
        print('[ERROR:] could not find created remote measurement folder!')
    return remotefolder


if __name__ == "__main__":

    L = LambdaRemote("192.168.253.17", "xspadmin", "l3tm3w0rk")

    # %% config

    # settings
    num_images = 5
    images_per_file = 1000  # Default = 1000
    filetype = ".h5"  # Default = ".h5"
    exposure_time_ms = 1000
    threshold_eV = 6000
    prefix = "test"

    # download folder
    save_path = "/data2/xraylab/measurements/lambda350k"

    # remote folder
    remote_path = "/home/xspadmin/tmp/data/"

    # scripts to run on detector
    config_script = "pylambda/remote/detconf_ownhdf5.py"
    h5_script = "pylambda/lmbd/hdf5writer.py"
    acq_script = "pylambda/remote/detacq_ownhdf5.py"
    local_script_path = "/data2/xraylab/scripts/pylabtest"
    remote_script_path = "/home/xspadmin/tmp/testscripts"

    # %% upload scripts

    L.upload_files(local_script_path, remote_script_path, [config_script, acq_script, h5_script])
    
    # %% config detector

    argv_config = str(exposure_time_ms) + " " + str(threshold_eV)
    _ = L.execute_script(remote_script_path, config_script.split("/")[-1], argv_config)

    # %% acquire images

    update_thl_eV = -1  # set -1 to not update THL
    verbose = False  # set False to turn off prints from SSH class in all acquisitions except the 1st.

    print('[INFO:] starting image acquisition ...')
    for i in range(num_images):

        print('\tacquisition {}/{} ...'.format(i + 1, num_images))

        # first acquisition: setup hdf5 files and acquire image
        if i == 0:
            # take first image and setup folder + master file
            argv_init = str(1) + " " + remote_path + " " + prefix + " " + str(exposure_time_ms) + " " + str(update_thl_eV) + " " + str(images_per_file) + " " + filetype

            # trigger acquisition script
            script_out = L.execute_script(remote_script_path, acq_script.split("/")[-1], argv_init)

            # get created folder
            remote_folder = extract_created_folder(script_out)

            # set 'remotepath' for all following acquisitions
            measurement_folder = os.path.join(remote_path, remote_folder)
            print('[DEBUG:] measurement folder:\t{}'.format(measurement_folder))

        # All following acquisitions: acquire images and write into existing file
        else:
            # take more images: provide 'measurement_folder' instead of 'remote_path'
            argv_acquire = str(0) + " " + measurement_folder + " " + prefix + " " + str(exposure_time_ms)
            L.execute_script(remote_script_path, acq_script.split("/")[-1], argv_acquire, verbose)

        # Do other stuff in between acquisitions
        # e.g. move motors
        print('\t-- placeholder --')
        # time.sleep(0.5)  # FIXME: replace with something useful

    print('[INFO:] Acquisition of {} images completed!'.format(num_images))

    # %% download all images from folder

    # make folder, identical name as on remote
    L.make_download_folder(save_path, remote_folder, no_unique_name=True)  # folder name with date and ID is already provided
    download_folder = os.path.join(save_path, L.downloadfolder)

    # download all files with FILTYPE as extension from MEASUREMENT_FOLDER
    L.download_from_directory(measurement_folder, download_folder, filetype)
