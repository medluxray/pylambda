#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 15 14:31:00 2020

@brief: Console app to upload a specific file from Lambda detector.

@author: Till, till.dreier@med.lu.se
"""

from lmbd.lmbdssh import LambdaRemote as lmbd

import os
import sys
import argparse


def parse_argv():
    """ parse console argv parameters """
    parser = argparse.ArgumentParser(description="Upload a specific file from a remote host via SSH")
    parser.add_argument("filename", help="Filename incl extension", type=str)
    parser.add_argument("locapath", help="Folder on local host containing the file to upload", type=str)
    parser.add_argument("remotepath", help="Folder on remote host to upload the file into", type=str)
    return parser.parse_args()


if __name__ == "__main__":
    """
    USAGE:
    run "python lmbd_ulfile.py filename.extension /path/to/local/folder /path/to/remote/folder
    
    Standard login and addresses are used.
    filename.extension is the file incl. extension to upload-
    /path/to/local/folder is the path to the directory containing the file to upload.
    /path/to/remote/folder is the path to the folder on the remote host to upload the file to. 
    """

    # parse input argument
    # filename = sys.argv[1]
    # localpath = sys.argv[2]
    # remotefolder = sys.argv[3]
    argv = parse_argv()

    # create lambda object with default params
    host = "192.168.253.17"
    user = "xspadmin"
    pasw = "l3tm3w0rk"

    L = lmbd(host, user, pasw)

    # upload specified file
    remotefpath = os.path.join(argv.remotefolder, argv.filename)
    lfpath = os.path.join(argv.localpath, argv.filename)
    print('[Upload:]\n\tFrom:\t{}\n\tTo:\t{}\n\tFile:\t{}'.format(lfpath, argv.remotefolder, argv.filename))

    try:
        L.upload_file(argv.localpath, remotefpath)
    finally:
        if L.ssh is not None:
            L.__close_ssh__()

    print('[INFO:] Done!')
