#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 15 14:11:00 2020

@brief: Console app to download a specific file from Lambda detector.

@author: Till, till.dreier@med.lu.se
"""

from lmbd.lmbdssh import LambdaRemote as lmbd

import os
# import sys
import argparse


def parse_argv():
    """ parse console argv parameters """
    parser = argparse.ArgumentParser(description="Download a specific file from a remote host via SSH")
    parser.add_argument("filename", help="Filename incl extension", type=str)
    parser.add_argument("remotepath", help="Folder on remote host containing the file to download", type=str)
    parser.add_argument("downloadfolder", help="Folder at /data2/xraylab/measurements/lambda350k/ to save downloaded file into. Optional. Default: YYYY-MM-DD_download_000x", type=str, nargs='?', const="download", default="download")
    return parser.parse_args()


if __name__ == "__main__":
    """
    USAGE:
    run "python lmbd_dlfile.py filename.extension /path/to/remote/folder/ download_folder_name
    Where standard login and addresses are used and donwload_folder_name
    refers to the folder to create in /data2/xraylab/measurements/lambda350k/. 
    """

    # parse input arguments
    # filename = sys.argv[1]  # file name incl. extension
    # remotepath = sys.argv[2]  # folder on remote host to download from
    # downloadfolder = sys.argv[3]  # name of the local folder to download to (will be created in /data2/xraylab/measurements/lambda350k/)
    argv = parse_argv()

    # create lambda object with default params
    host = "192.168.253.17"
    user = "xspadmin"
    pasw = "l3tm3w0rk"

    L = lmbd(host, user, pasw)

    # create download folder
    dlpath = "/data2/xraylab/measurements/lambda350k"
    L.make_download_folder(dlpath, argv.downloadfolder)

    # download specified file
    dlfolder = L.downloadfolder
    rfpath = os.path.join(argv.remotepath, argv.filename)
    print('[DOWNLOAD:]\n\tFrom:\t{}\n\tTo:\t{}\n\tFile:\t{}'.format(argv.remotepath, dlfolder, argv.filename))

    try:
        L.download_file(argv.filename, argv.remotepath, dlfolder)
    finally:
        if L.ssh is not None:
            L.__close_ssh__()

    print('[INFO:] Done!')
