#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 15 14:26:00 2020

@brief: Console app to download all .nxs files from a directory from Lambda detector.

@author: Till, till.dreier@med.lu.se
"""

from lmbd.lmbdssh import LambdaRemote as lmbd

import os
import sys
import argparse


def parse_argv():
    """ parse console argv parameters """
    parser = argparse.ArgumentParser(description="Download files from a directory on a remote host via SSH")
    parser.add_argument("remotedir", help="Directory to search for files", type=str)
    parser.add_argument("downloaddir", help="Local folder (in /data2/xraylab/measurements/lambda350k) to download files to", type=str)
    parser.add_argument("fileextension", help="Extension of the files to download. Default: .nxs, can be None to download all files.", type=str, nargs='?', const=".nxs", default=".nxs")
    return parser.parse_args()


if __name__ == "__main__":
    """
    USAGE:
    run "python lmbd_dldir.py /path/to/remote/directory download_folder_name
    Where standard login and addresses are used and donwload_folder_name
    refers to the folder to create in /data2/xraylab/measurements/lambda350k/. 
    """

    # parse input arguments
    # remotedir = sys.argv[1]
    # downloaddir = sys.argv[2]
    # fileextension = ".nxs"  # FIXME: make optional using argparse library instead of sys.argv
    argv = parse_argv()

    # create lambda object with default params
    host = "192.168.253.17"
    user = "xspadmin"
    pasw = "l3tm3w0rk"

    L = lmbd(host, user, pasw)

    # create download folder
    dlpath = "/data2/xraylab/measurements/lambda350k"
    L.make_download_folder(dlpath, argv.downloaddir)

    # download specified file
    dlfolder = L.downloadfolder
    print('[DOWNLOAD:]\n\tFrom:\t{}\n\tTo:\t{}'.format(argv.remotedir, dlfolder))

    try:
        L.download_from_directory(argv.remotedir, dlfolder, argv.fileextension)
    finally:
        if L.ssh is not None:
            L.__close_ssh__()

    print('[INFO:] Done!')
