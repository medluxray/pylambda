# pyLAMBDA

SSH Remote controller for an X-Spectrum Lambda Detector. Control the readout computer from another system on the same network. Contains acquisition and analysis libraries and some example scripts. Tested with a *GaAs* Lambda350k.

## Description

Scripts are copied to the remote control PC and triggered via SSH. The scripts are split into a configuration and acquisition scripts. Since there is no software trigger in the API, configuration is separated so it only has to be executed once, while the acquisition script can be run as often as desired.

The detector can be configured in different modes (24-bit, Dual-Threshold, and 24-bit Charge-summed are implemented). 24-bit and charge-summed images can be acquired as TIFF or packed in an HDF5 file. Dual-Threshold images can only be acquired in an HDF5 file. Generally, acquiring multiple images packed in HDF5 is beneficial. Libraries to open the created files are provided. The structure of the HDF5 files is similar to the structure of files acquired with an EIGER detector (since I have been working with one and could re-use my scripts this way).

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

The libraries in this project were developed using Python 3.7 via the Anaconda Distribution on Ubuntu 18.04 and MacOS Catalina. Windows compatibility was not tested.

The following packages should be installed by default in Anaconda:

```
Paramiko, numpy, scipy, skimage, PIL, h5py, threading, os, time, math, datetime, shutil, argparse
```

To handle compressed hdf5 files, the hdf5plugin module is used. This module is not part of the standard Anaconda install (https://pypi.org/project/hdf5plugin/). It can be installed using PIP inside the Anaconda environment. However, this step is not required since HDF5 files created by this library are not compressed and can be read via h5py directly.

```
pip install hdf5plugin
```

### Setting up the detector control PC

For scripts triggered via SSH, it is necessary to edit the .bashrc file on the remote to be able to use the Anaconda environment. Open the file and move the code loading Anaconda to the beginning of the file. Otherwise the scripts will be executed using the system Python, which is missing some required modules.

In the acquisition scripts, provide the correct IP address, user name and password.


## File Structure

Scripts in the home directory are example scripts. 
The *lmbd* subfolder contains the SSH controller for the detector and the HDF filewriter.
In *process* libraries to open and post-process the acquired data are provided.
The *remote* folder contains scripts that are copied to the remote PC and triggered via SSH.  

```
pylambda/
|- lmbd/
     |- hdf5writer.py
     |- lmbdssh.py
|- process/
     |- lambda_dpc.py
     |- lambda_filehandler.py
     |- lambda_imageproc.py
     |- lambda_phase.py
|- remote/
     |- detacq_dualthl.py
     |- detacq_imgnpy.py
     |- detacq_imgtiff.py
     |- detacq_ownhdf5.py
     |- detacq_simpleimg.py
     |- detconf_chargesum.py
     |- detconf_dualthl.py
     |- detconf_imgnpy.py
     |- detconf_imgtiff.py
     |- detconf_ownhdf5.py
     |- detconf_simpleimg.py
     |- detupd_exptime.py
     |- detupd_thl.py
     |- thl_scan.py
     |- xrayimage.py
|- acquire_hdf5_chargesum.py
|- acquire_hdf5_dualthl.py
|- acquire_hdf5_images.py
|- acquire_npy_image.py
|- acquire_tiff_image.py
|- analyse_single_grating.py
|- download_upload.py
|- lmbd_dldir.py
|- lmbd_dlfile.py
|- lmbd_ulfile.py
|- open_nxs_image.py
|- open_ownhdf5_image.py
|- open_ownhdf5_image_dualthl.py
|- run_remote_script.py
|- test_hdf5file.py
|- test_hdf5iterative.py
```

### Example Scripts (home directory)

The home directory contains a collection of scripts to acquire and view data.


Configure the detector in 24-bit charge-summed mode and acquire images packed in a HDF5 file:
```
acquire_hdf5_chargesum.py
```     

Configure the detector in Dual-Threshold mode and acquire images packed in a HDF5 file:
```
acquire_hdf5_dualthl.py       
```
Configure the detector in 24-bit mode and acquire images packed in a HDF5 file:

```
acquire_hdf5_images.py     
```

Configure the detector in 24-bit mode and acquire an image packed in a .npy file. This works without editing the .bashrc file on the remote. Acquired images have to be loaded and converted to TIFF after download:
```
acquire_npy_image.py  
```

Configure the detector in 24-bit mode an acquire TIFF images:
```
acquire_tiff_image.py
```

A script to extract dark-field and differential phase-contrast images using a single absorption grating setup (see https://iopscience.iop.org/article/10.1088/1361-6560/ab2ff5/meta and https://www.osapublishing.org/ol/abstract.cfm?uri=ol-35-12-1932):
```
analyse_singlegrating.py
```

Test script to download and upload files from/to the remote via SSH:
```
download_upload.py            
```

Terminal script to download all files from a directory on the remote via SSH:
```
lmdb_dldir.py
```

Terminal script to download a specific file from the remote via SSH:
```
lmbd_dlfile.py
```                

Terminal script to upload a specific file to the remote via SSH:
```
lmbd_ulfile.py
```

Open Lambda Nexus HDF5 file, created by the detector not by this library:
```
open_nxs_image.py
```             

Open HDF5 files created by this library:
```
open_ownhdf5_image.py
```         

Open HDF5 files created by this library. Plots lower and upper threshold image:
```
open_ownhdf5_image_dualthl.py
``` 

Test script to trigger a script on the remote via SSH:
```
run_remote_script.py
```          

Test the *Hdf5File* library:
```
test_hdf5file.py
```

Test the *Hdf5Iterative* library (used by the acquisition scripts):
```
test_hdf5iterative.py
```         

### lmbd Subfolder

Contains the *LambdaRemote* class used to download/upload files and trigger scripts via SSH:
```
lmbdssh.py
```

Contrains the *Hdf5File* and *Hdf5Iterative* classes used to write HDF5 files:
```
hdf5writer.py
```

The *hdf5writer.py* script is copied to the remote by scripts writing HDF5 files.

### process Subfolder

This folder contains libraries to open and process acquired data. Generally, the processing classes are inherited by the *filehandler*, i.e. only the correct *filehandler* has to be imported for processing.

Contains the *ImageSingleGrating* class to analyse single grating images (loaded from TIFF):
```
lambda_dpc.py
```

Contains the *LambdaFiles* class handling loading of HDF5 files, cropping, filtering of dead/noisy/masked pixels, normalisation, and saving of images. The *LambdaSRFiles* class adds image registration and interpolation functions. The *LambdaDPCFiles* class adds support for single grating images (same as *lambda_dpc.py* but integrated into the filehandler.):
```
lambda_filehandler.py
```

Contains the *ImageRegister* class, which is inherited by the *LambdaSRFiles* class providing image registration functions. The *ImageInterpolate* class is inherited by the *LambdaSRFiles* class as well and provides image interpolation. *ImageFilter* provides functions to remove dead, noisy, and masked pixels used by *LambdaFiles*. The *FilterOverUnderResponsive* class implements a different approach to identify over and under responsive pixels based on their surrounding pixels. However, this approach is not used and the class is untested. The functions have been tested before moving them into a class:
```
lambda_imageproc.py
```

Contains the *ImagePhase* class implementing Paganin phase-retrieval, which is inherited by *LambdaFiles*:
```
lambda_phase.py
```


### remote Subfolder

Scripts in this folder are copied to the remote host and triggered via SSH.

Acquisition script for Dual-Threshold mode:
```
detacq_dualthl.py
```

Acquisition script for image packed as numpy array:
```
detacq_imgnpy.py
```

Acquisition script for TIFF images:
```
detacq_imgtiff.py
```

Acquisition script creating and adding images into a HDF5 file:
```
detacq_ownhdf5.py
```

Acqusition script using the Lambda's HDF5 format. Creates 1 file per call:
```
detacq_simpleimg.py
```

Configuration script for charge-summed mode. Acquisition can be done via detacq_imgtiff.py or detacq_ownhdf5.py:
```
detconf_chargesum.py
```

Configuration script for Dual-Threshold mode. Acquisition via detacq_ownhdf5.py:
```
detconf_dualthl.py
```

Configuration script to acquire images as numpy array. Acquisition via detacq_imgnpy.py:
```
detconf_imgnpy.py
```

Configuration script to acquire TIFF images. Acquisition via detacq_imgtiff.py:
```
detconf_imgtiff.py
```

Configuration script to acquire a Lambda Nexus file. No iterative writing, will contain the amount of images set in this script. Acquisition via detacq_simpleimg.py:
```
detconf_simpleimg.py
```

Script to update the exposure time without reconfiguring the detector:
```
detupd_exptime.py
```

Script to update the energy threshold without reconfiguring the detector. This is also built into the acquisition scripts for TIFF and HDF5:
```
detupd_thl.py
```

Threshold scan example script from user manual:
```
thl_scan.py
```

Test script to configure detector and acquire an image. Modified version of threshold scan example:
```
xrayimage.py
```


## Authors

* **Till Dreier** - *Initial work* - [GitLab](https://gitlab.com/tilldreier)


## Acknowledgments

* **Julian Schmehr** (*X-Spectrum*) - For providing examples, additional information, and help troubleshooting.
* **Sascha Grimm** (*Dectris*) [GitHub](https://github.com/SaschaAndresGrimm) - Inspiration for the HDF5 file structure and writing of files.
