#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 16 20:02:00 2020

@brief: Take a single image as numpy array.
        Upload detconf_imgnpy and detacq_imgnpy scripts,
        Configure and run the detector,
        download resulting numpy data and convert to tiff.

        This script works without editing the .bashrc file on the remote.

@author: Till, till.dreier@med.lu.se
"""

from pylambda.lmbd.lmbdssh import LambdaRemote
from PIL import Image
import numpy as np
import os


if __name__ == "__main__":

    L = LambdaRemote("192.168.253.17", "xspadmin", "l3tm3w0rk")

    #%% config

    # settings
    exposure_time_ms = 5000
    threshold_eV = 6000
    savename = "test"

    # download folder
    save_path = "/data2/xraylab/measurements/lambda350k"

    # remote folder
    remote_folder = "/home/xspadmin/tmp/" + L.__get_date__() + "_" + savename

    # scripts to run on detector
    config_script = "detconf_imgnpy.py"
    acq_script = "detacq_imgnpy.py"
    local_script_path = "pylambda/remote"  # FIXME: the absolute path could be required (depends on editor)
    remote_script_path = "/home/xspadmin/tmp/testscripts"


    #%% upload scripts

    L.upload_files(local_script_path, remote_script_path, [config_script, acq_script])


    #%% config detector

    argv_config = str(exposure_time_ms) + " " + str(threshold_eV)
    L.execute_script(remote_script_path, config_script, argv_config)


    #%% take image

    argv_acquire = remote_folder + " " + str(exposure_time_ms)

    # ---- update the energy threshold ------------------------------------------------------------------------
    # new_energy_thl = 6500
    # argv_acquire = remote_folder + " " + str(exposure_time_ms) + " " + str(new_energy_thl)
    # ---------------------------------------------------------------------------------------------------------

    L.execute_script(remote_script_path, acq_script, argv_acquire)


    #%% download all images from folder
    
    # make folder
    L.make_download_folder(save_path, savename)
    download_folder = L.downloadfolder
    
    # download files
    L.download_from_directory(remote_folder, download_folder, ".npy")
    
    
    #%% convert to tiff
    
    flist = os.listdir(download_folder)
    for file in flist:
        if ".npy" in file:
            fpath = os.path.join(download_folder, file)
            tmp_img = Image.fromarray(np.load(fpath))
            spath = os.path.join(download_folder, file.split(".")[0] + ".tiff")
            tmp_img.save(spath)
            print('Saved:\t\t{}'.format(spath))
            os.remove(fpath)
            print('Removed:\t{}'.format(fpath))
