#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 15 14:05:00 2020

@brief: Classes to handle SSH connection, file download/upload to/from Lambda Detector, remote triggering of scripts, ...

@author: Till, till.dreier@med.lu.se
"""

import os
import paramiko
from datetime import datetime


class SSHcon:
    """ """

    def __init__(self, host="192.168.253.17", user="xspadmin", pwd="l3tm3w0rk"):
        """ Setup SSH connection. """
        self.host = host
        self.user = user
        self.pwd = pwd
        self.ssh = None
        self.sftp = None


    def __create_ssh__(self, verbose=False):
        """ Create SSH client. """
        self.ssh = paramiko.SSHClient()
        self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        if verbose: print('[INFO:] created SSH object.')


    def __open_ssh__(self, fileaccess=False, verbose=False):
        """ Open SSH connection. """
        # make SSH object if it doesn't exist
        if self.ssh is None:
            self.__create_ssh__(verbose)

        # open SSH connection
        self.ssh.connect(self.host, username=self.user, password=self.pwd)
        if verbose: print('[INFO:] opened SSH connection.')
        if fileaccess is True:
            self.sftp = self.ssh.open_sftp()
            if verbose: print('[INFO:] opened SFTP connection.')


    def __close_ssh__(self, verbose=False):
        """ Close SSH connection """
        if self.sftp is not None:
            self.sftp.close()
            self.sftp = None
            if verbose: print('[INFO:] closed SFTP connection.')
        self.ssh.close()
        self.ssh = None
        if verbose: print('[INFO:] closed SSH connection.')


class LambdaRemote(SSHcon):
    """ """

    def __init__(self, host="192.168.253.17", user="xspadmin", pwd="l3tm3w0rk"):
        """ Download/upload files and run scripts via SSH. """
        SSHcon.__init__(self, host, user, pwd)
        self.downloadfolder = None


    def __get_date__(self):
        """ Get the current date as string YYYY-MM-DD. """
        dt = datetime.now()
        return dt.strftime("%Y-%m-%d")


    def make_download_folder(self, path, folder_name, overwrite=False, no_unique_name=False):
        """ Create the download folder. """
        if no_unique_name is False:
            scan_id = 1
            while True:
                m_folder = os.path.join(path, self.__get_date__() + "_" + folder_name + "_{:04d}".format(scan_id))
                if overwrite is True:
                    print('[WARNING:] folder might be overwritten.')
                    break
                else:
                    if os.path.exists(m_folder):
                        scan_id += 1
                    else:
                        break
        else:
            m_folder = folder_name

        try:
            os.mkdir(os.path.join(path, m_folder))
            print('[INFO:] created folder: {}'.format(m_folder))
        except:
            print('[INFO:] {} already exists.'.format(m_folder))
        self.downloadfolder = m_folder


    def download_file(self, file, remotepath, localpath, verbose=False):
        """
        Download file from remote host via SSH.
        FILE is the file name with extension to download.
        REMOTEPATH is the path to the folder on the remote host containing the file.
        LOCALPATH is the path to the local folder to which to download the file.
        """
        # open SSH and SFTP
        self.__open_ssh__(fileaccess=True, verbose=verbose)

        # Download file
        rfpath = os.path.join(remotepath, file)
        lfpath = os.path.join(localpath, file)
        try:
            self.sftp.get(rfpath, lfpath)
            print('[INFO:] downloaded {}\n\tFrom {}\n\tTo {}'.format(file, remotepath, localpath))

        finally:
            # close SFTP and SSH
            self.__close_ssh__(verbose)


    def upload_file(self, file, localpath, remotepath, verbose=False):
        """
        Upload file to remote host via SSH.
        FILE is the file name with extension to upload.
        LOCALPATH is the path to the local folder containing the file.
        REMOTEPATH is the path to the folder on the remote host.
        """
        # open SSH and SFTP
        self.__open_ssh__(fileaccess=True, verbose=verbose)

        # Upload file
        lfpath = os.path.join(localpath, file)
        rfpath = os.path.join(remotepath, file)
        try:
            self.sftp.put(lfpath, rfpath)
            print('[INFO:] uploaded {}\n\tFrom {}\n\tTo {}'.format(file, localpath, remotepath))

        finally:
            # close SFTP and SSH
            self.__close_ssh__(verbose)


    def download_from_directory(self, remotedir, localdir, extension=".nxs", verbose=False):
        """
        Download files in REMOTEDIR to LOCALDIR vis SSH.
        All files when extension=None, all of a types when e.g. extension='.nxs'.
        """
        # open SSH and SFTP
        self.__open_ssh__(fileaccess=True, verbose=verbose)

        # get file list
        print('remote dir to search: {}'.format(remotedir))
        flist = self.sftp.listdir(path=remotedir)
        print('[INFO:] found {} files or directories in {}'.format(len(flist), remotedir))
        print('[INFO:] downloading all files with extension: {}'.format(extension))

        # download files
        try:
            for file in flist:
                if extension in file or extension is None:
                    print('\tDownloading {} ...'.format(file))
                    rpath = os.path.join(remotedir, file)
                    lpath = os.path.join(localdir, file)
                    self.sftp.get(rpath, lpath)
        finally:
            self.__close_ssh__(verbose)


    def upload_files(self, localpath, remotepath, filelist, verbose=False):
        """ Upload a list of files from LOCALPATH to REMOTEPATH via SSH. """
        # open SSH and SFTP
        self.__open_ssh__(fileaccess=True, verbose=verbose)

        # upload files
        try:
            if type(filelist) not in [tuple, list]: filelist = list(filelist)
            for file in filelist:
                script_name = file.split("/")[-1]  # in case file contains a folder name as well
                rfpath = os.path.join(remotepath, script_name)
                lfpath = os.path.join(localpath, file)
                # print('[DEBUG:] trying to upload:\n\tFrom  {}\n\tTo:    {}'.format(lfpath, rfpath))
                self.sftp.put(lfpath, rfpath)
                print('[INFO:] uploaded {} to {}'.format(file, remotepath))

        finally:
            # close SFTP and SSH
            self.__close_ssh__(verbose)


    def __sshcommand__(self, cmd, verbose=False, print_output=False):
        """ Execute a command via SSH. Run in TRY, FINALLY block to close SSH. """
        if verbose is True:
            print('Command to execute:\t{}'.format(cmd))
        stdin, stdout, stderr = self.ssh.exec_command(cmd)
        out = stdout.readlines()
        err = stderr.readlines()
        if print_output is True:
            print("Errors: ", err)
            print("Output: ", out)
        return out, err


    def execute_script(self, remotepath, script, argv="", verbose=False, print_output=False):
        """
        Execute script on remote host (at path REMOTEPATH) via SSH.
        SCRIPT needs to contain the file extension (.py).
        ARGV is a string containing the input parameters separated by a space.
        Returns output of remote script
        """
        # open SSH connection
        self.__open_ssh__(verbose=verbose)

        # prepare command
        command = "python " + os.path.join(remotepath, script) + " " + argv

        # run command
        try:
            out, err = self.__sshcommand__(command, verbose, print_output)
        finally:
            self.__close_ssh__(verbose)

        return out
                
    
    def delete_remote_directory(self, remotepath, verbose=False):
        """ delete folder on remote host via SSH. """
        self.__open_ssh__(fileaccess=True, verbose=verbose)
        cmd = "rm -rf " + remotepath
        
        try:
            self.__sshcommand__(cmd, verbose, verbose)
        finally:
            self.__close_ssh__(verbose)
            
            
    def make_remote_directory(self, remotepath, verbose=False):
        """ create a folder on remote host via SSH. """
        self.__open_ssh__(fileaccess=True, verbose=verbose)
        cmd = "mkdir " + remotepath
        
        try:
            self.__sshcommand__(cmd, verbose, verbose)
        finally:
            self.__close_ssh__(verbose)
