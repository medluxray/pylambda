#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 17 16:53:00 2020

@brief: Class creating HDF5 file on the detector.

@author: Till, till.dreier@med.lu.se
"""

import os
import numpy as np
# import hdf5plugin
import h5py
import threading
import time


# nodes and params to create in HDF5 file (these classes only write to "/entry/data/")
# structure is inspired by EIGER data (so I can re-use my existing code)
nodes = {   "entry" :               {"entry" : "entry", "attrs" : {'NX_class': 'NXentry'}},
            "data" :                {"entry" : "entry/data", "attrs" : {'NX_class': 'NXdata'}},
            "instrument" :          {"entry" : "entry/instrument", "attrs" : {'NX_class': 'NXinstrument'}},
            "detector":             {"entry" : "entry/instrument/detector", "attrs": {'NX_class': 'NXdetector'}},
            "geometry" :            {"entry" : "entry/instrument/geometry", "attrs" : {'NX_class': 'NXgeometry'}},
            "motors" :              {"entry" : "entry/instrument/motors", "attrs" : {'NX_class': 'NXmotors'}},
            "source" :              {"entry" : "entry/instrument/source", "attrs" : {'NX_class': 'NXsource'}}
         }

# FIXME: fill in params on local host after downloading the files
params = {  "exposure_time":        {"entry": "entry/instrument/detector/exposure_time", "attrs": {u'units': 'ms'}},
            "compression":          {"entry": "entry/instrument/detector/compression", "attrs": {}},
            "threshold":            {"entry": "entry/instrument/detector/threshold", "attrs": {u'units': 'eV'}},

            "source_object" :       {"entry" : "entry/instrument/geometry/source_object", "attrs" : {u'units': 'meter'}},
            "source_detector" :     {"entry" : "entry/instrument/geometry/source_detector", "attrs" : {u'units': 'meter'}},

            "motor_1" :             {"entry" : "entry/instrument/motors/motor_1", "attrs" : {u'units': 'mm'}},
            "motor_2" :             {"entry" : "entry/instrument/motors/motor_2", "attrs" : {u'units': 'mm'}},
            "motor_3" :             {"entry" : "entry/instrument/motors/motor_3", "attrs" : {u'units': 'deg'}},
            "motor_4" :             {"entry" : "entry/instrument/motors/motor_4", "attrs" : {u'units': 'mm'}},
            "motor_5" :             {"entry" : "entry/instrument/motors/motor_5", "attrs" : {u'units': 'mm'}},
            "motor_6" :             {"entry" : "entry/instrument/motors/motor_6", "attrs" : {u'units': 'mm'}},

            "target_material" :     {"entry" : "entry/instrument/source/target_material", "attrs" : {}},
            "acceleration_voltage": {"entry" : "entry/instrument/source/acceleration_voltage", "attrs" : {u'units': 'V'}},
            "emission_current":     {"entry" : "entry/instrument/source/emission_current", "attrs" : {u'units': 'A'}},
            "target_current":       {"entry" : "entry/instrument/source/target_current", "attrs" : {u'units': 'A'}},
            "emission_power":       {"entry" : "entry/instrument/source/emission_power", "attrs" : {u'units': 'W'}},
            "target_power":         {"entry" : "entry/instrument/source/target_power", "attrs" : {u'units': 'W'}}
         }





class Hdf5File:
    """ """

    def __init__(self, path, basename, ftype='.h5', imgs_per_file=1000):
        """
        Collect a buffer of numpy arrays and write to HDF5 file, link data files to master.
        """
        # input variables
        self.path = path
        self.basename = basename
        self.ftype = ftype
        self.imgs_per_file = imgs_per_file

        # define other variables
        self.master_file = None
        self.nr_files = 0
        self.data_buffer = []


    def create_master(self):
        """
        Create a master file.
        Cannot overwrite, make sure the folder does not contain a master file or delete it first.
        """
        self.master_file = os.path.join(self.path, "{}_master{}".format(self.basename, self.ftype))

        with h5py.File(self.master_file, 'w') as f:
            for key, entry in nodes.items():
                try:
                    group = f.create_group(entry["entry"])
                except ValueError:
                    group = f[entry["entry"]]

                for attribute, value in entry["attrs"].items():
                    group.attrs[attribute] = value
            print("Master file created:\t{}".format(self.master_file))


    def set_master(self, master_file):
        """ Set the name of an existing master file. """
        self.master_file = master_file
        print('Set existing master file {} at path {}'.format(self.master_file, self.path))


    def append_data(self, data=None):
        """ append an image of shape (1, x, y) to data_buffer """
        if data is not None:
            self.data_buffer.append(data)

        if len(self.data_buffer) >= self.imgs_per_file:
            print('[INFO:] Writing buffer to file ...')
            self.nr_files += 1
            threading.Thread(target=self.__write_to_file__, args=(self.data_buffer,)).start()
            self.data_buffer = []  # empty data_buffer


    def write_remaining(self):
        """ write the remaining data from self.data_buffer to file. """
        if len(self.data_buffer) > 0:
            print('[INFO:] Writing remaining images to file ...')
            self.nr_files += 1
            threading.Thread(target=self.__write_to_file__, args=(self.data_buffer,)).start()
            self.data_buffer = []  # empty data_buffer
        else:
            print('[INFO:] data_buffer is already empty!')


    def __write_to_file__(self, buffer):
        """ write data buffer (list of numpy arrays of shape (1, x, y) to hdf5 file and link to master. """
        # file name
        data_name = "data_{:05}".format(self.nr_files)
        dfile = "{}_{}{}".format(self.basename, data_name, self.ftype)
        fpath = os.path.join(self.path, dfile)

        # write file
        with h5py.File(fpath, 'w') as f:
            data = np.vstack(buffer)
            dset = f.create_dataset("/entry/data/data", data=data, chunks=(1,) + data.shape[-2:])

            f["entry"].attrs['NX_class'] = 'NXentry'
            f["entry/data"].attrs['NX_class'] = 'NXdata'
            dset.attrs["image_nr_high"] = np.shape(data)[0]
            dset.attrs["image_nr_low"] = 1

        print('[INFO:] Created file:\t{}'.format(fpath))

        # link to master
        with h5py.File(self.master_file, "a") as f:
            f["/entry/data/" + data_name] = h5py.ExternalLink(dfile, "/entry/data/data/")
            f["/entry/data"].attrs["NX_class"] = "NXdata"
            f["/entry"].attrs["NX_class"] = "NXentry"

        print('[INFO:] Linked {} to master {}'.format(dfile, self.master_file))


class Hdf5Iterative(Hdf5File):
    """ """

    def __init__(self, path, basename, ftype='.h5', imgs_per_file=1000):
        """
        Write images (numpy arrays of shape (N,x,y)) into hdf5 file iteratively, N image at a time.
        USAGE:
            path                path to folder in which to create h5 files.
            basename            file prefix of h5 files, index will be appended.
            ftype               file extension, default: .h5
            imgs_per_file       number of images per file.
        """
        Hdf5File.__init__(self, path, basename, ftype, imgs_per_file)

        # other variables
        self.master_file = None
        self.filenum = 0
        self.data_files = []


    def __get_num_datafiles__(self):
        """ Return the amount of existing data files """
        self.data_files = []
        for file in os.listdir(self.path):
            if "data" in file:
                self.data_files.append(file)
        self.data_files.sort()  # make sure the files are sorted by their appended index
        return len(self.data_files)


    def __get_num_registered_datafiles__(self):
        """ Return the amount of registered data files (in master file) """
        with h5py.File(os.path.join(self.path, self.master_file), 'r') as f:
            dset = f["/entry/data/"]
            return len(dset)


    def __get_current_datafile__(self):
        """ Return the current datafile """
        _ = self.__get_num_datafiles__()  # just to update self.data_files
        return self.data_files[-1]


    def __get_num_images_in_current_datafile__(self, h5dataprefix="data"):
        """ Return the amount of images in the current data file (the one at the highest index in self.data_files) """
        last_dfile = self.__get_num_registered_datafiles__()
        if last_dfile == 0:
            return 0
        last_dname = "{}_{:05}".format(h5dataprefix, last_dfile - 1)
        with h5py.File(os.path.join(self.path, self.master_file), 'r') as f:
            dset = f["/entry/data/{}".format(last_dname)]
            return dset.shape[0]


    def __new_datafile_name__(self):
        """ Return valid name for a new data file. """
        idx = 0
        name_valid = False
        while not name_valid:
            dfile_name = "{}_data_{:05}{}".format(self.basename, idx, self.ftype)
            if os.path.isfile(os.path.join(self.path, dfile_name)):
                idx += 1
            else:
                name_valid = True
        return dfile_name


    def append_param(self, node, value):
        """
        Insert a node into HDF5 master file. Append if already exists.
        Node is e.g.  params['motor_1']['entry'] or "/entry/instrument/motors/motor_1".
        Value can be int or float, or a list thereof.
        """
        if type(value) is str:
            print('CRITICAL: cannot write strings into file with this method!')
        if type(value) is np.ndarray:
            value = value.tolist()
        elif type(value) is not list:
            value = [value]

        with h5py.File(os.path.join(self.path, self.master_file), "a") as f:

            # if node does not exist yet - create
            if node not in f:
                f.create_dataset(node, data=value, chunks=True, maxshape=(None,))
                print('DEBUG: created note: {}'.format(node))

            # if node exists - append
            else:
                dset = f[node]
                current_len = len(dset)
                dset.resize((current_len + len(value), ))
                dset[current_len:] = value
                print('DEBUG: written {} into {}'.format(value, node))


    def read_param(self, node):
        """
        Read node from HDF5 master file.
        Node is e.g.  params['motor_1']['entry'] or "/entry/instrument/motors/motor_1".
        """
        with h5py.File(os.path.join(self.path, self.master_file), "r") as f:

            # if node does not exist
            if node not in f:
                print('WARNING: node {} does not exist in file {}.'.format(node, self.master_file))
                return None

            # if node exists
            else:
                dset = f[node]
                return dset[:]


    def __delete_node__(self, node):
        """
        Delete a node.
        Node is e.g.  params['motor_1']['entry'] or "/entry/instrument/motors/motor_1".
        """
        with h5py.File(os.path.join(self.path, self.master_file), "a") as f:
            if node in f:
                del f[node]
                print('INFO: deleted node {}'.format(node))
            else:
                print('WARNING: node {} does not exist.'.format(node))


    def append_to_file(self, image, h5path="/entry/data/data"):
        """ Append an image to file as 3D numpy array (1,x,y), create file if it doesn't exist. """
        # check if input array has correct shape
        if len(image.shape) != 3: print('ERROR: provide image as 3D numpy array with shape (N,x,y)!')

        # check if master file is set
        if self.master_file is None: print('ERROR: master file not set!')

        # figure out how many data files exist and how many images are in the latest one
        num_dfiles = self.__get_num_datafiles__()
        num_imginfile = 0
        if num_dfiles > 0:
            num_imginfile = self.__get_num_images_in_current_datafile__(h5path.split("/")[-1])

        # create new data file (if non exists or full)
        if num_dfiles == 0 or num_imginfile >= self.imgs_per_file:
            df_name = self.__new_datafile_name__()
            with h5py.File(os.path.join(self.path, df_name), 'w') as f:
                _, xshape, yshape = image.shape
                f.create_dataset(h5path, data=image, chunks=True, maxshape=(None, xshape, yshape))
            print('[INFO:] created new data file: \t{}'.format(df_name))
            time.sleep(0.5)  # give the computer some time to create the file

            # link to master
            with h5py.File(os.path.join(self.path, self.master_file), "a") as f:
                f[h5path + "_{:05}".format(num_dfiles)] = h5py.ExternalLink(df_name, h5path + "/")
            print('[INFO:] linked new data file to master')

        # append to file (check if there is still space in current data file)
        elif num_imginfile < self.imgs_per_file:
            current_data_file = self.__get_current_datafile__()
            with h5py.File(os.path.join(self.path, current_data_file), 'a') as f:
                f[h5path].resize((f[h5path].shape[0] + image.shape[0]), axis=0)
                f[h5path][-image.shape[0]:] = image
            print('Image(s) appended to file:  {}'.format(current_data_file))

        else:
            # should never be reached
            print('ERROR: unhandled case in  append_to_file()')





