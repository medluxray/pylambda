#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 12 13:22:47 2020

@brief:  Trigger a script on the remote host via SSH.
         Function has been integrated into lmbdssh!

@author: Till, till.dreier@med.lu.se
"""

import os
import paramiko


def execute_remote_script(path, script, argv):
    """ """    
    host = "192.168.253.17"
    user = "xspadmin"
    pwd = "l3tm3w0rk"
    
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(host, username=user, password=pwd)
    print('[INFO:] SSH connection established.')
    
    command = "python " + os.path.join(path, script) + " " + argv
    print('Command to execute:  {}'.format(command))
    
    try:
        stdin, stdout, stderr = ssh.exec_command(command)
        print("stderr: ", stderr.readlines())
        print("pwd: ", stdout.readlines())
    
    finally:
        ssh.close()
        print('[INFO:] SSH connection closed.')
    
    
    
if __name__ == "__main__":
        
    #%% detconf_simpleimg
    path = "/home/xspadmin/tmp/testscripts/"
    script = "detconf_simpleimg.py"
    scan_name = "roots"
    remote_spath = "/home/xspadmin/tmp/linkoping/"
    exposure_time_ms = 5000
    thl_energy_kev = 6.5
    argv = " " + scan_name + " " + remote_spath + " " + str(exposure_time_ms) + " " + str(thl_energy_kev)
    
    execute_remote_script(path, script, argv)
    
    
    #%% detacq_simpleimg
    path = "/home/xspadmin/tmp/testscripts/"
    script = "detacq_simpleimg.py"
    exposure_time_ms = 5000
    argv = " " + str(exposure_time_ms)
    
    execute_remote_script(path, script, argv)