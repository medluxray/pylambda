#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 6 09:00:00 2020

@brief:  Analyse single grating data.

@author: Till, till.dreier@med.lu.se
"""

from pylambda.process.lambda_dpc import ImageSingleGrating

import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm


if __name__ == "__main__":

    # path and files
    path = "/Users/tildre/Data/Lambda350k/2020-06-29_pulpdry"
    datafile = "sample.tiff"
    flatfile = "flat.tiff"
    trueflatfile = "true_flat.tiff"
    maskfile = "mask_xlpixels.tiff"

    # load images
    I = ImageSingleGrating()
    sample = I.load_image(path, datafile)
    reference = I.load_image(path, flatfile)
    trueflat = I.load_image(path, trueflatfile)
    mask = I.load_image(path, maskfile).astype(bool)

    # correct images
    img = I.trueflat_correction(sample, trueflat)
    ref = I.trueflat_correction(reference, trueflat)

    # filter images (remove outliers, dead, noisy)
    img, _ = I.prepare_and_apply_mask(img, outliers_thl=3, dead_thl=0.1, verbose=True)
    ref, _ = I.prepare_and_apply_mask(ref, outliers_thl=3, dead_thl=0.1, verbose=True)

    # remove XL pixels using a loaded mask
    img = I.remove_masked(img, mask=mask)
    ref = I.remove_masked(ref, mask=mask)

    # plot image and reference
    plt.imshow(img, norm=LogNorm())
    plt.title("Corrected image")
    plt.show()

    plt.imshow(ref, norm=LogNorm())
    plt.title("Corrected reference")
    plt.show()

    plt.imshow(abs(np.fft.fftshift(np.fft.fft2(img))), norm=LogNorm())
    plt.title("FFT of corrected image")
    plt.show()

    # extract modalities
    abs, dfl, dfr, dpcl, dpcr = I.extract_modalities(img, ref, pixels_per_period=3, tolerance=1, verbose=True)

    savepath = os.path.join(path, "result")
    I.save_to_tiff(abs, savepath, "abs.tiff")
    I.save_to_tiff(dfl, savepath, "dfl.tiff")
    I.save_to_tiff(dfr, savepath, "dfr.tiff")
    I.save_to_tiff(dpcl, savepath, "dpcl.tiff")
    I.save_to_tiff(dpcr, savepath, "dpcr.tiff")


