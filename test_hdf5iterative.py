#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 18 12:22:00 2020

@brief: Test class (Hdf5Iterative) for creating HDF5 files and appending images iteratively.
        The file is closed after every image (image stack) was written.
        NOTE: This does not test the class on the remote host!

@author: Till, till.dreier@med.lu.se
"""

from lmbd.hdf5writer import Hdf5Iterative
import numpy as np


if __name__ == "__main__":

    path = "/Users/tildre/Data/Lambda350k"
    file_base = "testiter"
    file_type = ".h5"
    images_per_file = 1000

    # create master file
    h5 = Hdf5Iterative(path, file_base, file_type, images_per_file)
    h5.create_master()
    master_file = h5.master_file

    for _ in range(25):
        # make random data
        tmp = np.random.random((1, 2*256, 3*256)) * 100
        tmp = tmp.astype(np.uint32)

        # re-create object
        htmp = Hdf5Iterative(path, file_base, file_type, images_per_file)
        htmp.set_master(master_file)

        # append to file
        htmp.append_to_file(tmp)


    print('DONE!')
