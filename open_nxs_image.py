#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 12 14:02:07 2020

@brief:  Open, plot and save images from a Lambda Nexus file (.nxs)

@author: Till, till.dreier@med.lu.se
"""

import os
import numpy as np
from PIL import Image
import hdf5plugin
import h5py
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm


def save_to_tiff(image, path, folder, filename):
    tmp = Image.fromarray(image.astype(np.float32))
    tmp.save(os.path.join(path, folder, filename))
    print("Saved image '{}' to {}".format(filename, os.path.join(path, folder)))


if __name__ == "__main__":
    
    path = "/data2/xraylab/measurements/lambda350k"
    folder = "2020-06-16_linkoping_0006"
    file = "roots_simpleimg_00002.nxs"
    
    
    fpath = os.path.join(path, folder, file)
    
    with h5py.File(fpath, 'r') as f:
        dset = f["/entry/instrument/detector/data"]
        img = np.average(dset[:], axis=0)

plt.imshow(img, norm=LogNorm())
plt.colorbar()
plt.show()

#sname = "sample_5s.tiff"
#save_to_tiff(img, path, folder, sname)