#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 12 09:50:26 2020

@brief:  Test script to upload and download scripts.
         Contains prototype of SSH class, now integrated into lmbd/lmbdssh.py !!!

@author: Till, till.dreier@med.lu.se
"""

import os
import paramiko
from datetime import datetime


class lambdassh:
    """ """
    
    def __init__(self, host="192.168.253.17", user="xspadmin", pwd="l3tm3w0rk"):
        """ Setup SSH connection. """
        self.host = host
        self.user = user
        self.pwd = pwd
        self.ssh = None
        self.sftp = None
    

    def __create_ssh__(self):
        """ Create SSH client. """
        self.ssh = paramiko.SSHClient()
        self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        print('[INFO:] created SSH object')


    def __open_ssh__(self):
        """ Open SSH connection. """
        self.ssh.connect(self.host, username=self.user, password=self.pwd)
        self.sftp = self.ssh.open_sftp()
        print('[INFO:] opened SSH connection')
                
    
    def __close_ssh__(self):
        """ Close SSH connection """
        self.sftp.close()
        self.ssh.close()
        self.ssh = None
        self.sftp = None
        print('[INFO:] closed SSH connection.')






class lambdaupdown(lambdassh):
    """ """

    def __init__(self, host="192.168.253.17", user="xspadmin", pwd="l3tm3w0rk"):
        """ Download and upload files via SSH. """
        lambdassh.__init__(self, host, user, pwd)
        self.downloadfolder = None


    def __get_date__(self):
        """ Get the current date as string YYYY-MM-DD. """
        dt = datetime.now()
        return dt.strftime("%Y-%m-%d")


    def make_download_folder(self, path, folder_name, overwrite=False):
        """ Create the download folder. """
        scan_id = 1
        while True:
            m_folder = os.path.join(path, self.__get_date__() + "_" + folder_name + "_{:04d}".format(scan_id))
            if overwrite is True:
                print('[WARNING:] folder might be overwritten.')
                break
            else:
                if os.path.exists(m_folder):
                    scan_id += 1
                else:
                    break

        os.mkdir(m_folder)
        self.downloadfolder = m_folder
        print('[INFO:] created folder: {}'.format(m_folder))


    def download_file(self, remotepath, localpath):
        """ Download file from remote host via SSH.  """
        # open SSH and SFTP
        if self.ssh is None:
            self.__create_ssh__()
        self.__open_ssh__()

        # Download file
        self.sftp.get(remotepath, localpath)
        print('[INFO:] downloaded file to {}'.format(localpath))

        # close SFTP and SSH
        self.__close_ssh__()


    def upload_file(self, localpath, remotepath):
        """ Upload file to remote host via SSH. """
        # open SSH and SFTP
        if self.ssh is None:
            self.__create_ssh__()
        self.__open_ssh__()

        # Upload file
        self.sftp.put(localpath, remotepath)
        print('[INFO:] uploaded file to {}'.format(remotepath))

        # close SFTP and SSH
        self.__close_ssh__()


    def download_from_directory(self, remotedir, localdir, ending=".nxs"):
        """ Donwload all files in directory via SSH. """
        # open SSH and SFTP
        if self.ssh is None:
            self.__create_ssh__()
        self.__open_ssh__()

        # get file list
        flist = self.sftp.listdir(path=remotedir)
        print('[INFO: found {} files or directories in {}'.format(len(flist), remotedir))
        for file in flist:
            if ending in file:
                print('Downloading from {} into {}'.format(remotedir, localdir))
                rpath = os.path.join(remotedir, file)
                lpath = os.path.join(localdir, file)
                self.sftp.get(rpath, lpath)





if __name__ == "__main__":
    
    L = lambdaupdown()
    
    
    #%% DOWNLOAD
    try:
        
        # paths
        remote_path = "/home/xspadmin/tmp/testimages"
        remote_file = "flat_1s_XrayScan_00000.nxs"
        download_path = "/data2/xraylab/measurements/lambda350k"    
        download_folder = "test"
        
        # make folder
        L.make_download_folder(download_path, download_folder)
        
        # download file
        rpath = os.path.join(remote_path, remote_file)
        dpath = os.path.join(L.downloadfolder, remote_file)
        L.download_file(rpath, dpath)
        
    finally:
        # if SSH was not closed, close it before script finishes
        if L.ssh is not None:
            print('[INFO:] cleaning up SSH connection at script end.')
            L.__close_ssh__()
        print('DONE!')
        
        
    #%% UPLOAD
    try:
        # paths
        local_path = "/data2/xraylab/scripts/pylambdatest/remote"
        local_file = "xrayimage.py"
        remote_path = "/home/xspadmin/tmp/testscripts"
        
        lpath = os.path.join(local_path, local_file)
        rpath = os.path.join(remote_path, local_file)
        
        # upload file
        L.upload_file(lpath, rpath)
        
    finally:
        # if SSH was not closed, close it before script finishes
        if L.ssh is not None:
            print('[INFO:] cleaning up SSH connection at script end.')
            L.__close_ssh__()
        print('DONE!')



