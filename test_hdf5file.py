#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 17 12:39:00 2020

@brief: Test class (Hdf5File) for creating HDF5 files on local host.
        Creates a buffer that is written to file when full.
        NOTE: This does not test the class on the remote host!

@author: Till, till.dreier@med.lu.se
"""

from lmbd.hdf5writer import Hdf5File
import numpy as np


if __name__ == "__main__":

    path = "/Users/tildre/Data/Lambda350k"
    file_base = "test_v2"
    file_type = ".h5"
    images_per_file = 10

    h5 = Hdf5File(path, file_base, file_type, images_per_file)
    h5.create_master()

    for _ in range(25):
        # make random data
        tmp = np.random.random((1, 2*256, 3*256)) * 100
        tmp = tmp.astype(np.uint32)

        # append test data
        h5.append_data(tmp)

    # write remaining buffered data
    h5.write_remaining()

    print('DONE! (note that filewriter thread might not have finished yet!')
