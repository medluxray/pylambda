#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 23 09:20:45 2020

@brief:  Open, plot and save images from HDF5 files. Plots lower and upper threshold.

@author: Till, till.dreier@med.lu.se
"""

import os
import numpy as np
from PIL import Image
import hdf5plugin
import h5py
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm


def save_to_tiff(image, path, folder, filename):
    tmp = Image.fromarray(image.astype(np.float32))
    tmp.save(os.path.join(path, folder, filename))
    print("Saved image '{}' to {}".format(filename, os.path.join(path, folder)))


if __name__ == "__main__":
    
    path = "/data2/xraylab/measurements/lambda350k"
    folder = "2020-07-29_dualthl_0002"
    
    # find master file
    master_file = None
    for file in os.listdir(os.path.join(path, folder)):
        if "master" in file:
            master_file = file
    
    # load all images
    fpath = os.path.join(path, folder, master_file)
    with h5py.File(fpath, 'r') as f:
        dset = f["/entry/data/data_00000"]
        imgs = dset[:]


index = 2  # has to be even!
plt.imshow(imgs[index], norm=LogNorm())
plt.title("Lower at idx={}".format(index))
plt.colorbar()
plt.show()

plt.imshow(imgs[index + 1], norm=LogNorm())
plt.title("Upper at idx={}".format(index + 1))
plt.colorbar()
plt.show()

#sname = "sample_5s.tiff"
#save_to_tiff(img, path, folder, sname)