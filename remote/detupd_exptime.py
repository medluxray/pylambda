#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 26 09:52:00 2020

@brief: Update exposure time.

@author: Till, till.dreier@med.lu.se
"""

import PyTango
import time
import argparse


def parse_argv():
    """ parse console argv parameters """
    parser = argparse.ArgumentParser(description="Update exposure time on an already configured detector.")
    parser.add_argument("exposuretime", help="Exposure time (in ms)", type=int)
    return parser.parse_args()


def update_exposuretime(expo_time_ms):
    """ Configure the detector to acquire and image without writing it to a file. """

    # connect to detector
    found = False
    cnt = 0
    while not found and cnt < 20:
        try:
            print("\b.")
            lmbd = PyTango.DeviceProxy("//xspserver:10000/xsp/lambda/01")  # need to change the DeviceProxy address to match what is installed in your PC
            time.sleep(0.5)
            if lmbd.state() == PyTango.DevState.ON:
                found = True
                print("found 750k device")
                configfolder = "/opt/xsp/config/"  # this needs to point to the config folder
        except:
            found = False
            print("exception ocurred on 750k device")

        print("")

    # set exposure time
    t_exp = int(expo_time_ms)
    lmbd.set_timeout_millis(t_exp + 10000)  # timeout time, needs to be longer than exposure time

    # detector settings
    lmbd.ShutterTime = t_exp

    time.sleep(0.1)
    print('Exposure time set to {} ms'.format(t_exp))




if __name__ == '__main__':

    argv = parse_argv()

    update_exposuretime(argv.exposuretime)
