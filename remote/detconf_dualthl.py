#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 22 09:20:00 2020

@brief: Configure detector to acquire an image in dual-threshold mode.
        Creates a hdf5 file on detector, from which the 2 images have to be extracted.

@author: Till, till.dreier@med.lu.se
"""

import PyTango
import time
import os
import argparse


def parse_argv():
    """ parse console argv parameters """
    parser = argparse.ArgumentParser(description="Acquire an image via lmbd.LiveLastImageData without creating a HDF5 file.")
    parser.add_argument("dumpdir", help="Dump directory for nxs files.", type=str)
    parser.add_argument("exposuretime", help="Exposure time (in ms)", type=int)
    parser.add_argument("thl_lower", help="Lower energy threshold (in eV). Minimum: 5 keV. Default: 5 keV", type=int, nargs='?', const=6000, default=6000)
    parser.add_argument("thl_upper", help="Upper energy threshold (in eV). Minimum 12 keV. Default: 12 keV", type=int, nargs='?', const=12000, default=12000)
    return parser.parse_args()


def __make_folder__(fpath):
    """ Create folder if not exist """
    # make folder name
    if os.path.isdir(fpath):
        print('Folder already exists.')
    else:
        os.mkdir(fpath)
        print('Folder created: {}'.format(fpath))
        

def config_ownhdf5(dumpdir, expo_time_ms=1000, thl_lower_ev=5000, thl_upper_ev=12000):
    """ Configure the detector to acquire and image without writing it to a file. """

    # connect to detector
    found = False
    cnt = 0
    while not found and cnt < 20:
        try:
            print("\b.")
            lmbd = PyTango.DeviceProxy("//xspserver:10000/xsp/lambda/01")  # need to change the DeviceProxy address to match what is installed in your PC
            time.sleep(0.5)
            if lmbd.state() == PyTango.DevState.ON:
                found = True
                print("found 750k device")
                configfolder = "/opt/xsp/config/"  # this needs to point to the config folder
        except:
            found = False
            print("exception ocurred on 750k device")

        print("")

    # set exposure time
    t_exp = int(expo_time_ms)
    print('Exposure time set to {} ms'.format(t_exp))
    lmbd.set_timeout_millis(t_exp + 10000)  # timeout time, needs to be longer than exposure time


    # dump dir on remote
    __make_folder__(dumpdir)

    # detector settings    
    lmbd.SaveFilePath = dumpdir
    lmbd.FrameNumbers = 1
    lmbd.ShutterTime = t_exp
    lmbd.FilePrefix = "dualthl"
    lmbd.FileStartNum = 0
    lmbd.SaveAllImages = True

    time.sleep(5)

    # set operating mode
    print("Changing operating mode...")
    lmbd.write_attribute("OperatingMode", "DualThreshold")
    time.sleep(2)

    # set lower energy threshold (min 5 keV)
    energy_scale = 1000.0
    thl_lower_energy = int(thl_lower_ev)  # lower threshold
    thl_upper_energy = int(thl_upper_ev)  # upper threshold

    lmbd.ThreadNo = 0
    print('[DEBUG:] current ThreadNo: {}'.format(lmbd.ThreadNo))
    lmbd.EnergyThreshold = thl_lower_energy
    print("Lower Energy Threshold set to {} keV".format(thl_lower_energy / energy_scale))

    time.sleep(0.1)
    print('[DEBUG:] lower threshold readback: {} eV'.format(lmbd.EnergyThreshold))

    # set upper energy threshold (min 12 keV)
    lmbd.ThreadNo = 1
    print('[DEBUG:] current ThreadNo: {}'.format(lmbd.ThreadNo))
    lmbd.EnergyThreshold = thl_upper_energy
    print("Upper Energy Threshold set to {} keV".format(thl_upper_energy / energy_scale))

    time.sleep(0.1)  # Have to have short delay to ensure commands execute in right order - setting energy and starting acq have different polling loops in Tango server, so without the time gap the execution becomes unpredictable
    print('[DEBUG:] upper threshold readback: {} eV'.format(lmbd.EnergyThreshold))





if __name__ == '__main__':

    argv = parse_argv()

    config_ownhdf5(argv.dumpdir, argv.exposuretime, argv.thl_lower, argv.thl_upper)
