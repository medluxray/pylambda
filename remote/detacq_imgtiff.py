#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun 20 16:12:00 2020

@brief: Acquire single image and write to TIFF.
        Run this after config using detconf_imgtiff.py script.
        Save path on remote host and Exposure time [ms] needs to be provided.

@author: Till, till.dreier@med.lu.se
"""

import PyTango
import time
import sys
import os
import numpy as np
import argparse
from PIL import Image  # not installed on remote host
# import imageio  # not installed on remote host


def parse_argv():
    """ parse console argv parameters """
    parser = argparse.ArgumentParser(
        description="Acquire an image via lmbd.LiveLastImageData without creating a HDF5 file.")
    parser.add_argument("savepath", help="Folder on remote host to write image to", type=str)
    parser.add_argument("exposuretime", help="Exposure time (in ms)", type=int)
    parser.add_argument("threshold", help="Optional. Update energy threshold (in eV). Default: -1 (do not update)",
                        type=int, nargs='?', const=-1, default=-1)
    return parser.parse_args()


def __make_folder__(path):
    """ Create folder """
    if os.path.isdir(path):
        print('Save folder already exists.')
    else:
        os.mkdir(path)
        print('Created save folder: {}'.format(path))


def __create_img_name__(path, fname):
    """ Return a valid file name for the given folder """
    idx = 0
    name_valid = False
    while name_valid is False:
        imgname = fname + "_{:05}.tiff".format(idx)
        if os.path.isfile(os.path.join(path, imgname)):
            idx += 1
        else:
            name_valid = True
    return imgname


def __write_to_tiff__(image, path, fname):
    """ Write numpy array to file, append 5 digits """
    # PIL version
    img_name = __create_img_name__(path, fname)
    spath = os.path.join(path, img_name)
    img = Image.fromarray(image)
    img.save(spath)
    # IMAGEIO version
    #    img_name = __create_img_name__(path, fname)
    #    spath = os.path.join(path, img_name)
    #    imageio.imwrite(spath, image)
    return spath


def save_image(image, path, fname='image'):
    """
    Write image to TIFF.
    Make sure the image file has a unique digit number appended.
    """
    spath = __write_to_tiff__(image, path, fname)
    print('Saved {}'.format(spath))


def acquire_liveimage(remotepath, exp_time_ms=1000, energy_thl=-1):
    """
    Acquire an image as numpy array with exposure time EXP_TIME_MS in ms.
    Set energy_thl [in eV] it the energy threshold should be updated. Default: -1 --> do not update thl.
    """

    # Re-connect to Lambda device and start acquisition
    print('Connecting to detector ...')
    found = False
    cnt = 0
    while not found and cnt < 20:
        try:
            print("\b.")
            lmbd = PyTango.DeviceProxy(
                "//xspserver:10000/xsp/lambda/01")  # need to change the DeviceProxy address to match what is installed in your PC
            time.sleep(0.1)
            if lmbd.state() == PyTango.DevState.ON:
                found = True
                # print("found 750k device")
                configfolder = "/opt/xsp/config/"  # this needs to point to the config folder
        except:
            found = False
            print("exception ocurred on 750k device")
    print("")

    # update single energy threshold (if provided)
    if energy_thl > 0:
        energy_scale = 1000.0
        thl_energy = int(energy_thl)  # energy thl in eV
        lmbd.EnergyThreshold = thl_energy
        print("Energy Threshold updated: {} keV".format(thl_energy / energy_scale))
        time.sleep(0.1)

    # Acquire image
    print("Starting acquisition ...")
    lmbd.command_inout("StartAcq")

    found = False
    cnt = 0
    t_exp = int(exp_time_ms)
    time.sleep(t_exp / 1000)

    while not found and cnt < 100:
        time.sleep(0.5)
        cnt += 1
        if lmbd.state() == PyTango.DevState.ON:
            found = True
            break

        if cnt >= 100:
            print("Timeout!")
            break

    # save image to tiff
    img = np.array(lmbd.LiveLastImageData, dtype=np.int32)
    print('[DEBUG:] LiveImage: {}'.format(img.shape))
    save_image(img, remotepath)


if __name__ == "__main__":
    # save_path = sys.argv[1]  # save path on remote host
    # exposure_time_ms = sys.argv[2]  # exposure time in ms
    argv = parse_argv()

    print('Creating folder: {} ...'.format(argv.savepath))
    __make_folder__(argv.savepath)

    acquire_liveimage(argv.savepath, argv.exposuretime, argv.threshold)
