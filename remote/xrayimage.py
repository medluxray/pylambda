#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 12 13:06:21 2020

@author: Till, till.dreier@med.lu.se
"""

import PyTango
import time
import os
import sys

 
def xrayImage(detname, outdir, expo_time=1000):
        fileprefix = detname + "_XrayScan"

        # Define Tango device(s) (Lambda + Timer)
        #
        # init & wait for server

        found = False
        cnt = 0
        while not found and cnt < 20:
                try:
                        print("\b.")
                        # lmbd = PyTango.DeviceProxy("//xs-pclab01:10000/xsp/lambda/01")   #need to change the DeviceProxy address to match what is installed in your PC
                        lmbd = PyTango.DeviceProxy("//xspserver:10000/xsp/lambda/01")   #need to change the DeviceProxy address to match what is installed in your PC
                        time.sleep(0.5)
                        if lmbd.state() == PyTango.DevState.ON:
                                found = True
                                print("found 750k device")
                                configfolder = "/opt/xsp/config/" #this needs to point to the config folder
                except:
                        found = False
                        print("exception ocurred on 750k device")
                        
                print( "")


        # expoti = int(expo_time*1000) 	# in miliseconds
        expoti = int(expo_time)
        print('Exposure time set to {} ms'.format(expoti))

        lmbd.set_timeout_millis(expoti + 10000)  # timeout time

        """
        configure the detector with appropriate settings
        """
        ##print 'Setting parms '
        lmbd.SaveFilePath=outdir
        lmbd.FrameNumbers=1
        lmbd.ShutterTime=expoti
        lmbd.FilePrefix=fileprefix
        lmbd.FileStartNum=0
        lmbd.ThreadNo=0
        lmbd.SaveAllImages=True

        time.sleep(5)

        """
        Load appropriate operating mode.
        """
        print("Changing operating mode...")
        lmbd.write_attribute("OperatingMode", "TwentyFourBit")
        time.sleep(1)
        
        
        thl_energy = 6.5  # energy thl in eV
        energyScale = 1000.0
        lmbd.EnergyThreshold = thl_energy * energyScale
        print("Energy Threshold set to {} keV".format(thl_energy))
        
        time.sleep(1) # Have to have short delay to ensure commands execute in right order - setting energy and starting acq have different polling loops in Tango server, so without the time gap the execution becomes unpredictable
        
        # Acquire image
        print("Starting acquisition ...")
        lmbd.command_inout("StartAcq")

        found = False
        cnt = 0
        time.sleep(expoti / 1000)
        
        while not found and cnt < 100:
            time.sleep(0.5)
            cnt +=1
            if lmbd.state() == PyTango.DevState.ON:
                found = True
                break
                
        if cnt >= 100:
            print("Timeout!")

        print("Done!")


if __name__ == '__main__':
    inputdetname = sys.argv[1]
    inputoutdir = sys.argv[2]
    exposure_time_ms = sys.argv[3]
    xrayImage(inputdetname,inputoutdir, exposure_time_ms)