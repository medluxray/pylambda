#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 22 09:20:00 2020

@brief: Configure detector to acquire an image in charge-summed mode.
        Minimum energy threshold is 12 keV.
        For GaAs detector minimum lower thl is 7 keV, upper is 15 keV.
        Lower threshold is used to identify charge-sharing.
        Acquire images using detacq_ownhdf5.py script.

@author: Till, till.dreier@med.lu.se
"""

import PyTango
import time
import argparse


def parse_argv():
    """ parse console argv parameters """
    parser = argparse.ArgumentParser(description="Acquire an image via lmbd.LiveLastImageData without creating a HDF5 file.")
    parser.add_argument("exposuretime", help="Exposure time (in ms)", type=int)
    parser.add_argument("threshold", help="Energy threshold (in eV). Minimum 12 keV. Default: 15 keV", type=int, nargs='?', const=15000, default=15000)
    parser.add_argument("lower_thl", help="Lower energy threshold (in eV) used to find charge sharing. Minimum 5 keV. Default: 7 keV", type=int, nargs='?', const=7000, default=7000)
    return parser.parse_args()


def config_ownhdf5(expo_time_ms=1000, threshold_ev=15000, lower_thl=7000):
    """ Configure the detector to acquire and image without writing it to a file. """

    # connect to detector
    found = False
    cnt = 0
    while not found and cnt < 20:
        try:
            print("\b.")
            lmbd = PyTango.DeviceProxy("//xspserver:10000/xsp/lambda/01")  # need to change the DeviceProxy address to match what is installed in your PC
            time.sleep(0.5)
            if lmbd.state() == PyTango.DevState.ON:
                found = True
                print("found 750k device")
                configfolder = "/opt/xsp/config/"  # this needs to point to the config folder
        except:
            found = False
            print("exception ocurred on 750k device")

        print("")

    # set exposure time
    t_exp = int(expo_time_ms)
    print('Exposure time set to {} ms'.format(t_exp))
    lmbd.set_timeout_millis(t_exp + 10000)  # timeout time, needs to be longer than exposure time



    # detector settings
    lmbd.SaveFilePath = "/home/xspadmin/tmp/"
    lmbd.FrameNumbers = 1
    lmbd.ShutterTime = t_exp
    lmbd.FilePrefix = "image"
    lmbd.FileStartNum = 0
    lmbd.SaveAllImages = False

    time.sleep(2)


    # set operating mode
    print("Changing operating mode...")
    lmbd.write_attribute("OperatingMode", "TwentyFourBitCSM")
    time.sleep(2)


    # set lower energy threshold to 5 keV.
    energy_scale = 1000.0
    lmbd.ThreadNo = 0
    lmbd.EnergyThreshold = lower_thl    
    print("[DEBUG:] lower thl readback: {} eV".format(lmbd.EnergyThreshold))

    time.sleep(0.1)

    # set upper energy threshold (min 12 keV)
    thl_upper_energy = int(threshold_ev)  # upper threshold
    lmbd.ThreadNo = 1
    lmbd.EnergyThreshold = thl_upper_energy
    print("Energy Threshold set to {} keV".format(thl_upper_energy / energy_scale))
    print("[DEBUG:] upper thl readback: {} eV".format(lmbd.EnergyThreshold))

    time.sleep(0.1)  # Have to have short delay to ensure commands execute in right order - setting energy and starting acq have different polling loops in Tango server, so without the time gap the execution becomes unpredictable





if __name__ == '__main__':

    argv = parse_argv()

    config_ownhdf5(argv.exposuretime, argv.threshold, argv.lower_thl)
