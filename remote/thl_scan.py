import PyTango
import time
import os
import sys

 
def thlScan(detname, outdir):
        expo_time = 10  # in seconds
        fileprefix = detname + "_XrayScan_220to0"

        # Define Tango device(s) (Lambda + Timer)
        #
        # init & wait for server

        found = False
        cnt = 0
        while not found and cnt < 20:
                try:
                        print("\b.")
                        # lmbd = PyTango.DeviceProxy("//xs-pclab01:10000/xsp/lambda/01")   #need to change the DeviceProxy address to match what is installed in your PC
                        lmbd = PyTango.DeviceProxy("//xspserver:10000/xsp/lambda/01")   #need to change the DeviceProxy address to match what is installed in your PC
                        time.sleep(0.5)
                        if lmbd.state() == PyTango.DevState.ON:
                                found = True
                                print("found 750k device")
                                configfolder = "/opt/xsp/config/" #this needs to point to the config folder
                except:
                        found = False
                        print("exception ocurred on 750k device")
                        
                print( "")


        expoti = int(expo_time*1000) 	# in miliseconds

        lmbd.set_timeout_millis(expoti+1000)

        """
        configure the detector with appropriate settings
        """
        ##print 'Setting parms '
        lmbd.SaveFilePath=outdir
        lmbd.FrameNumbers=1
        lmbd.ShutterTime=expoti
        lmbd.FilePrefix=fileprefix
        lmbd.FileStartNum=0
        lmbd.ThreadNo=0
        lmbd.SaveAllImages=True

        time.sleep(5)

        """
        Load appropriate operating mode.
        """
        print("Changing operating mode...")
        lmbd.write_attribute("OperatingMode", "TwentyFourBit")
        time.sleep(10)
        

        startE = 220.001
        endE = 0.001
        stepE = -1.0
        stepsNeeded = int(round(1 + (endE-startE)/stepE))
        energyScale = 1000.0
        
        
        for i in range(0,stepsNeeded):
                currentE = startE + i * stepE
                print( "Energy "+str(currentE))
                lmbd.EnergyThreshold=currentE*energyScale
                time.sleep(0.1) # Have to have short delay to ensure commands execute in right order - setting energy and starting acq have different polling loops in Tango server, so without the time gap the execution becomes unpredictable
                lmbd.command_inout("StartAcq")

                found = False
                cnt = 0
                time.sleep(expo_time)
                while not found and cnt < 100:
                        time.sleep(0.5)
                        cnt +=1
                        if lmbd.state() == PyTango.DevState.ON:
                                found = True
                if cnt >= 100:
                        print( "\b Timeout - exiting loop")
                        break


        print("Done....")


if __name__ == '__main__':
    inputdetname = sys.argv[1]
    inputoutdir = sys.argv[2] 
    thlScan(inputdetname,inputoutdir)