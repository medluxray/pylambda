#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 16 14:33:00 2020

@brief: Config detector to acquire 1 image.
        Start acquisition with detacq_simpleimg.py script.
        File name prefix, save directory on remote host, exposure time [ms],
        and a single energy threshold [eV] have to be specified.

@author: Till, till.dreier@med.lu.se
"""

import PyTango
import time
import os
import sys


def config_simpleimage(prefix, outdir, expo_time_ms=1000, thl_ev=6500):
    """ """

    # file prefix, 5 digits will be added
    fileprefix = prefix + "_simpleimg"

    found = False
    cnt = 0
    while not found and cnt < 20:
        try:
            print("\b.")
            # lmbd = PyTango.DeviceProxy("//xs-pclab01:10000/xsp/lambda/01")   #need to change the DeviceProxy address to match what is installed in your PC
            lmbd = PyTango.DeviceProxy(
                "//xspserver:10000/xsp/lambda/01")  # need to change the DeviceProxy address to match what is installed in your PC
            time.sleep(0.5)
            if lmbd.state() == PyTango.DevState.ON:
                found = True
                print("found 750k device")
                configfolder = "/opt/xsp/config/"  # this needs to point to the config folder
        except:
            found = False
            print("exception ocurred on 750k device")

        print("")

    # t_exp = int(expo_time_ms*1000) 	# in miliseconds
    t_exp = int(expo_time_ms)
    print('Exposure time set to {} ms'.format(t_exp))

    lmbd.set_timeout_millis(t_exp + 10000)  # timeout time

    """
    configure the detector with appropriate settings
    """
    ##print 'Setting parms '
    lmbd.SaveFilePath = outdir
    lmbd.FrameNumbers = 1
    lmbd.ShutterTime = t_exp
    lmbd.FilePrefix = fileprefix
    lmbd.FileStartNum = 0
    lmbd.ThreadNo = 0
    lmbd.SaveAllImages = True

    time.sleep(5)

    """
    Load appropriate operating mode.
    """
    print("Changing operating mode...")
    lmbd.write_attribute("OperatingMode", "TwentyFourBit")
    time.sleep(2)

    energy_scale = 1000.0
    thl_energy = int(thl_ev)  # energy thl in eV
    lmbd.EnergyThreshold = thl_energy
    print("Energy Threshold set to {} keV".format(thl_energy / energy_scale))

    time.sleep(0.1)  # Have to have short delay to ensure commands execute in right order - setting energy and starting acq have different polling loops in Tango server, so without the time gap the execution becomes unpredictable


if __name__ == '__main__':

    file_prefix = sys.argv[1]  # prefix of the file to create
    output_dir = sys.argv[2]  # location on the detector PC to write file to
    exposure_time_ms = sys.argv[3]  # exposure time in ms
    thl_ev = sys.argv[4]  # energy threshold in eV

    config_simpleimage(file_prefix, output_dir, exposure_time_ms, thl_ev)
