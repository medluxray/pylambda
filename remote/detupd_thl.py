#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 26 09:55:00 2020

@brief: Update energy threshold

@author: Till, till.dreier@med.lu.se
"""

import PyTango
import time
import argparse


def parse_argv():
    """ parse console argv parameters """
    parser = argparse.ArgumentParser(description="Update energy threshold on an already configured detector.")
    parser.add_argument("threshold", help="Single energy threshold (in eV)", type=int)
    return parser.parse_args()


def config_ownhdf5(thl_ev):
    """ Configure the detector to acquire and image without writing it to a file. """

    # connect to detector
    found = False
    cnt = 0
    while not found and cnt < 20:
        try:
            print("\b.")
            lmbd = PyTango.DeviceProxy("//xspserver:10000/xsp/lambda/01")  # need to change the DeviceProxy address to match what is installed in your PC
            time.sleep(0.5)
            if lmbd.state() == PyTango.DevState.ON:
                found = True
                print("found 750k device")
                configfolder = "/opt/xsp/config/"  # this needs to point to the config folder
        except:
            found = False
            print("exception ocurred on 750k device")

        print("")

    # set single energy threshold
    energy_scale = 1000.0
    thl_energy = int(thl_ev)  # energy thl in eV
    lmbd.EnergyThreshold = thl_energy

    time.sleep(0.1)  # Have to have short delay to ensure commands execute in right order - setting energy and starting acq have different polling loops in Tango server, so without the time gap the execution becomes unpredictable
    print("Energy Threshold set to {} keV".format(thl_energy / energy_scale))





if __name__ == '__main__':

    argv = parse_argv()

    config_ownhdf5(argv.threshold)
