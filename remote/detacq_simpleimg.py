#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 16 14:33:00 2020

@brief: Acquire single image and write to HDF5.
        Run this after config using detconf_simpleimg.py script.
        Exposure time [ms] needs to be provided.

@author: Till, till.dreier@med.lu.se
"""

import PyTango
import time
import os
import sys


def acquire_simpleimage(exp_time_ms=1000):
    """ Re-connect to Lambda device and start acquisition"""

    print('Connecting to detector ...')
    found = False
    cnt = 0
    while not found and cnt < 20:
        try:
            print("\b.")
            lmbd = PyTango.DeviceProxy("//xspserver:10000/xsp/lambda/01")  # need to change the DeviceProxy address to match what is installed in your PC
            time.sleep(0.1)
            if lmbd.state() == PyTango.DevState.ON:
                found = True
                # print("found 750k device")
                configfolder = "/opt/xsp/config/"  # this needs to point to the config folder
        except:
            found = False
            print("exception ocurred on 750k device")
    print("")

    # Acquire image
    print("Starting acquisition ...")
    lmbd.command_inout("StartAcq")

    found = False
    cnt = 0
    t_exp = int(exp_time_ms)
    time.sleep(t_exp / 1000)

    while not found and cnt < 100:
        time.sleep(0.5)
        cnt += 1
        if lmbd.state() == PyTango.DevState.ON:
            found = True
            break

        if cnt >= 100:
            print("Timeout!")
            break

    print("Image acquired!")



if __name__ == "__main__":

    exposure_time_ms = sys.argv[1]  # exposure time in ms

    acquire_simpleimage(exposure_time_ms)
