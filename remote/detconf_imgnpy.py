#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 16 18:55:00 2020

@brief: Configure detector to acquire an image that won't be saved to HDF5.
        Image needs to be grabbed and saved via np.array(lmbd.LiveLastImageData)
        Exposure time [ms], and a single energy threshold [eV] have to be specified.

@author: Till, till.dreier@med.lu.se
"""

import PyTango
import time
import sys
import argparse


def parse_argv():
    """ parse console argv parameters """
    parser = argparse.ArgumentParser(description="Acquire an image via lmbd.LiveLastImageData without creating a HDF5 file.")
    parser.add_argument("exposuretime", help="Exposure time (in ms)", type=int)
    parser.add_argument("threshold", help="Single energy threshold (in eV). Default: 6 keV", type=int, nargs='?', const=6000, default=6000)
    return parser.parse_args()


def config_imgnpy(expo_time_ms=1000, thl_ev=6500):
    """ Configure the detector to acquire and image without writing it to a file. """

    # connect to detector
    found = False
    cnt = 0
    while not found and cnt < 20:
        try:
            print("\b.")
            lmbd = PyTango.DeviceProxy("//xspserver:10000/xsp/lambda/01")  # need to change the DeviceProxy address to match what is installed in your PC
            time.sleep(0.5)
            if lmbd.state() == PyTango.DevState.ON:
                found = True
                print("found 750k device")
                configfolder = "/opt/xsp/config/"  # this needs to point to the config folder
        except:
            found = False
            print("exception ocurred on 750k device")

        print("")

    # set exposure time
    t_exp = int(expo_time_ms)
    print('Exposure time set to {} ms'.format(t_exp))
    lmbd.set_timeout_millis(t_exp + 10000)  # timeout time, needs to be longer than exposure time

    # detector settings
    lmbd.SaveFilePath = "/home/xspadmin/tmp/"
    lmbd.FrameNumbers = 1
    lmbd.ShutterTime = t_exp
    lmbd.FilePrefix = 'image'
    lmbd.FileStartNum = 0
    lmbd.ThreadNo = 0
    lmbd.SaveAllImages = False

    time.sleep(5)

    # set operating mode
    print("Changing operating mode...")
    lmbd.write_attribute("OperatingMode", "TwentyFourBit")
    time.sleep(2)

    # set single energy threshold
    energy_scale = 1000.0
    thl_energy = int(thl_ev)  # energy thl in eV
    lmbd.EnergyThreshold = thl_energy
    print("Energy Threshold set to {} keV".format(thl_energy / energy_scale))

    time.sleep(0.1)  # Have to have short delay to ensure commands execute in right order - setting energy and starting acq have different polling loops in Tango server, so without the time gap the execution becomes unpredictable


if __name__ == '__main__':

    # exposure_time_ms = sys.argv[1]  # exposure time in ms
    # thl_ev = sys.argv[2]  # energy threshold in eV
    argv = parse_argv()

    config_imgnpy(argv.exposuretime, argv.threshold)
