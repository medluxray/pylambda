#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 17 11:38:00 2020

@brief: Acquire images and write to HDF5.
        DESCRIPTION

@author: Till, till.dreier@med.lu.se
"""

# FIXME: remove lmbd to run this on remote host!
from hdf5writer import Hdf5Iterative as h5i

import PyTango
import time
import os
import numpy as np
import argparse
import shutil
from datetime import datetime
# from PIL import Image  # not installed on remote host
# import imageio  # not installed on remote host


def parse_argv():
    """ parse console argv parameters """
    parser = argparse.ArgumentParser(description="Acquire an image via lmbd.LiveLastImageData without creating a HDF5 file.")
    parser.add_argument("newmeasurement", help="Set 1 when starting a new measurement", type=int)
    parser.add_argument("remotepath", help="Path to location where to create measurement folder when starting new measurement or path to the created measurement folder when continuing a measurement", type=str)
    parser.add_argument("basename", help="Filename prefix for h5 file (and its containing folder when creating new measurement)", type=str)
    parser.add_argument("exposuretime", help="Exposure time (in ms)", type=int)
    parser.add_argument("threshold", help="Optional. Update energy threshold (in eV). Default: -1 (do not update)", type=int, nargs='?', const=-1, default=-1)
    parser.add_argument("imagesperfile", help="Optional. Number of images per file. Default: 1000 (ca. 1.5 GB)", type=int, nargs='?', const=1000, default=1000)
    parser.add_argument("filetype", help="Optional. File extension. Default: .h5", type=str, nargs='?', const=".h5", default=".h5")
    return parser.parse_args()


def acquire_ownhdf5(h5obj, exp_time_ms=1000, energy_thl=-1):
    """
    Acquire an image as numpy array with exposure time EXP_TIME_MS in ms.
    Set energy_thl [in eV] it the energy threshold should be updated. Default: -1 --> do not update thl.
    """

    # Re-connect to Lambda device and start acquisition
    print('Connecting to detector ...')
    found = False
    cnt = 0
    while not found and cnt < 20:
        try:
            print("\b.")
            lmbd = PyTango.DeviceProxy(
                "//xspserver:10000/xsp/lambda/01")  # need to change the DeviceProxy address to match what is installed in your PC
            time.sleep(0.1)
            if lmbd.state() == PyTango.DevState.ON:
                found = True
                # print("found 750k device")
                configfolder = "/opt/xsp/config/"  # this needs to point to the config folder
        except:
            found = False
            print("exception ocurred on 750k device")
    print("")

    # update single energy threshold (if provided)
    if energy_thl > 0:
        energy_scale = 1000.0
        thl_energy = int(energy_thl)  # energy thl in eV
        lmbd.EnergyThreshold = thl_energy
        print("Energy Threshold updated: {} keV".format(thl_energy / energy_scale))
        time.sleep(0.1)

    # Acquire image
    print("Starting acquisition ...")
    lmbd.command_inout("StartAcq")

    found = False
    cnt = 0
    t_exp = int(exp_time_ms)
    time.sleep(t_exp / 1000)

    while not found and cnt < 100:
        time.sleep(0.5)
        cnt += 1
        if lmbd.state() == PyTango.DevState.ON:
            found = True
            break

        if cnt >= 100:
            print("Timeout!")
            break

    # get last image
    img = np.array(lmbd.LiveLastImageData, dtype=np.uint32)
    print('[DEBUG:] acquired image shape: {}'.format(img.shape))

    # live image is 2D array, make 3D array of shape (N,x,y)
    if len(img.shape) == 2:
        shp = (1, img.shape[0], img.shape[1])
        img = img.reshape(shp)
        print('[DEBUG:] image shape converted: {}'.format(img.shape))

    # save image to hdf5
    h5obj.append_to_file(img)


def __get_date__():
    """ Get the current date as string YYYY-MM-DD. """
    dt = datetime.now()
    return dt.strftime("%Y-%m-%d")


def __remove_folder__(dirpath):
    """ Delete a folder and all its content """
    try:
        shutil.rmtree(dirpath)
    except OSError as e:
        print("[Error:] {} - {}.".format(e.filename, e.strerror))


def __make_folder__(path, dirname):
    """ Create unique folder with current date prepended and a 4-digit index appended """
    # make folder name
    idx = 0
    created = False
    while not created:
        folder = "{}_{}_{:04}".format(__get_date__(), dirname, idx)
        fpath = os.path.join(path, folder)
        if os.path.isdir(fpath):
            idx += 1
        else:
            os.mkdir(fpath)
            created = True
    print('FOLDER: {}'.format(fpath))  # this can be found in the stdout argument list
    return fpath





if __name__ == "__main__":

    # parse input arguments
    argv = parse_argv()

    # check if starting a new measurement
    new_measurement = True if argv.newmeasurement == 1 else False  # convert newmeasurement argument to bool

    # NEW MEASUREMENT
    if new_measurement is True:
        # make new folder (/remotepath/YYYY-MM-DD_basename_XXXX where XXXX is a 4-digit unique index)
        # NOTE: when continuing the measurement, argv.remotepath should include the created folder name!
        print('Creating folder ...')
        rpath = __make_folder__(argv.remotepath, argv.basename)

        # make h5 file
        H = h5i(rpath, argv.basename, argv.filetype, argv.imagesperfile)
        H.create_master()


    # CONTINUE MEASUREMENT
    else:
        # re-create object and set existing master file
        # NOTE: argv.remotepath must include the created folder name!
        H = h5i(argv.remotepath, argv.basename, argv.filetype, argv.imagesperfile)
        master_file = os.path.join(argv.remotepath, "{}_master{}".format(argv.basename, argv.filetype))
        H.set_master(master_file)  # master_file contains the full path to the master file


    # acquire image
    acquire_ownhdf5(H, argv.exposuretime, argv.threshold)
