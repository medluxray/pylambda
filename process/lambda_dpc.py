#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jul 5 17:05:00 2020

@brief:  Extract absorption, dark-field, and differential phase-contrast images for single grating data.
         Inherits ImageFilter to remove outliers, dead, noisy, and masked pixels.

@author: Till, till.dreier@med.lu.se
"""

from pylambda.process.lambda_imageproc import ImageFilter

import os
import math
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from skimage.feature import peak_local_max


class ImageSingleGrating(ImageFilter):
    """ """


    def __init__(self):
        """ """
        ImageFilter.__init__(self)


    @staticmethod
    def load_image(path, file):
        """ Load an image from path """
        image = np.array(Image.open(os.path.join(path, file)))
        return image


    @staticmethod
    def load_images_from_folder(path, extension=".tiff"):
        """ Load all images with extension from path """
        # get files with ending in path
        files = [f for f in os.listdir(path) if f.endswith(extension)]
        # get amount of files and image shape
        num_files = len(files)
        imgshape = Image.open(os.path.join(path, files[0])).size
        # allocate space
        arr = np.zeros((num_files, imgshape[0], imgshape[1]))
        # load all images
        for idx, file in enumerate(files):
            arr[idx] = np.array(Image.open(os.path.join(path, file)))
        return arr


    @staticmethod
    def save_to_tiff(image, path, fname):
        """ Save image to tiff. """
        tmp = Image.fromarray(image)
        tmp.save(os.path.join(path, fname))


    def trueflat_correction(self, image, trueflat):
        """ """
        return image / trueflat


    def __check_equal_regions__(self, lst):
        return lst[1:] == lst[:-1]


    def __wrap_to_pi__(self, phase_arr):
        """
        Wrap an array of phases of an array of complex numbers to [-pi, +pi].
        :param phase_arr:   Array of phases (obtained via np.angle(arr)).
        :return:            Array of phases wrapped to pi.
        """
        return np.arctan2(np.sin(phase_arr), np.cos(phase_arr))


    def extract_modalities(self, image, ref, pixels_per_period=4, correct_thickness=True,
                           verbose=False, tolerance=0, min_distance='auto'):
        """
        Extract the modalities (absorption, dark-field (left+right), phase-contrast (left+right)) from Fourier space.
        :param image:                   Image containing sample and grating as 2D numpy array corrected with true_flat.
        :param ref:                     Reference image containing only the grating as 2D numpy array.
        :param pixels_per_period:       Pixels per grating period, depends on the grating alignment.
        :param correct_thickness:       Apply -ln to the modalities. See H.Wen et al (2010).
        :param verbose:                 When True, print coordinates of maxima, show assigned peaks in Fourier
                                        space, and print size of the extracted regions.
        :param tolerance:               Reduce region size by this amount of pixels. Regions should have the
                                        same size. Size might vary slightly if alignment is not perfect.
        :param min_distance:            Minimum distance (in pixels) of the peaks in Fourier space. Set 'auto' to select
                                        a working value for pixels_per_period 4 and 3 (haven't tested any other)
                                        or provide an int.
        """
        x_size, y_size = image.shape  # NOTE: x = columns, y = rows

        # size of areas to extract
        if verbose:
            print('[INFO:] reducing region size by {}. If all regions do not have equal sizes, try increasing the tolerance.'.format(tolerance))
        area_x = math.floor(x_size / pixels_per_period / 2) - tolerance
        area_y = math.floor(y_size / pixels_per_period / 2) - tolerance

        # make sure the area size is even to allow the maximum value to be exactly in the centre
        if area_x % 2 != 0:
            area_x -= 1
        if area_y % 2 != 0:
            area_y -= 1

        # Fourier transform
        fft_img = np.fft.fftshift(np.fft.fft2(image))
        fft_ref = np.fft.fftshift(np.fft.fft2(ref))

        # find local maxima (Fourier peaks)
        nr_of_maxima = 5
        if type(min_distance) is str and min_distance == 'auto':
            if pixels_per_period == 4:
                min_distance = math.ceil(y_size / 10)
            elif pixels_per_period == 3:
                min_distance = math.ceil(y_size / 20)
            else:
                min_distance = math.ceil(y_size / 20)  # FIXME
        elif type(min_distance) is int:
            min_distance = min_distance
        else:
            print("[ERROR:] invalid input for min_distance. Provide 'auto' or an integer.")

        coords_maxima = peak_local_max(abs(fft_img), min_distance=min_distance, num_peaks=nr_of_maxima)

        if verbose:
            print('[DEBUG:] minimum peak distance is: {} pixels'.format(min_distance))
            print('[DEBUG:] Coords maxima\n{}'.format(coords_maxima))


        # figure out which maximum belongs to which harmonic
        index_ctr, index_bl, index_br, index_tl, index_tr = -1, -1, -1, -1, -1
        for c in coords_maxima:
            if c[0] < x_size / 4 and c[1] > y_size / 4 * 3:
                if verbose: print('[DEBUG:] TOP RIGHT: {}'.format(c))
                index_tr = c
            elif c[0] < x_size / 4 and c[1] < y_size / 4:
                if verbose: print('[DEBUG:] TOP LEFT: {}'.format(c))
                index_tl = c
            elif c[0] > x_size / 4 * 3 and c[1] > y_size / 4 * 3:
                if verbose: print('[DEBUG:] BOTTOM RIGHT: {}'.format(c))
                index_br = c
            elif c[0] > x_size / 4 * 3 and c[1] < y_size / 4:
                if verbose: print('[DEBUG:] BOTTOM LEFT: {}'.format(c))
                index_bl = c
            elif c[0] < x_size / 4 * 3 and c[0] > x_size / 4 and c[1] > y_size / 4 and c[1] < y_size / 4 * 3:
                if verbose: print('[DEBUG:] CENTER: {}'.format(c))
                index_ctr = c
            else:
                if verbose: print('[ERROR:] INVALID MAXIMUM: {}'.format(c))

        # check if all indices were assigned
        if -1 in (item for sublist in [index_ctr, index_bl, index_br, index_tl, index_tr] for item in sublist):
            print('[ERROR:] Maxima have not been assigned correctly.')

        # DEBUG: show how the peaks have been assigned
        if verbose:
            plt.imshow(abs(fft_img), norm=LogNorm())
            plt.plot(index_ctr[1], index_ctr[0], 'o', label='Centre')
            plt.plot(index_tl[1], index_tl[0], 'o', label='Top Left')
            plt.plot(index_tr[1], index_tr[0], 'o', label='Top Right')
            plt.plot(index_bl[1], index_bl[0], 'o', label='Bottom Left')
            plt.plot(index_br[1], index_br[0], 'o', label='Bottom Right')
            plt.legend()
            plt.title("Fourier image with assigned local maxima")
            plt.show()

        # centre (0,0) - absorption image
        region_ctr_img = fft_img[index_ctr[0] - area_x:index_ctr[0] + area_x + 1, index_ctr[1] - area_y:index_ctr[1] + area_y + 1]
        region_ctr_ref = fft_ref[index_ctr[0] - area_x:index_ctr[0] + area_x + 1, index_ctr[1] - area_y:index_ctr[1] + area_y + 1]

        # top left (-1,1)
        region_tl_img = fft_img[index_tl[0] - area_x:index_tl[0] + area_x + 1, index_tl[1] - area_y:index_tl[1] + area_y + 1]
        region_tl_ref = fft_ref[index_tl[0] - area_x:index_tl[0] + area_x + 1, index_tl[1] - area_y:index_tl[1] + area_y + 1]

        # top right (1,1)
        region_tr_img = fft_img[index_tr[0] - area_x:index_tr[0] + area_x + 1, index_tr[1] - area_y:index_tr[1] + area_y + 1]
        region_tr_ref = fft_ref[index_tr[0] - area_x:index_tr[0] + area_x + 1, index_tr[1] - area_y:index_tr[1] + area_y + 1]

        # bottom left (-1,-1), identical to top right (1,1)
        region_bl_img = fft_img[index_bl[0] - area_x:index_bl[0] + area_x + 1, index_bl[1] - area_y:index_bl[1] + area_y + 1]
        region_bl_ref = fft_ref[index_bl[0] - area_x:index_bl[0] + area_x + 1, index_bl[1] - area_y:index_bl[1] + area_y + 1]

        # bottom right (1,-1), identical to top left (-1,1)
        region_br_img = fft_img[index_br[0] - area_x:index_br[0] + area_x + 1, index_br[1] - area_y:index_br[1] + area_y + 1]
        region_br_ref = fft_ref[index_br[0] - area_x:index_br[0] + area_x + 1, index_br[1] - area_y:index_br[1] + area_y + 1]

        # FIXME: add "fill_crop", add padding or reduce region size when slicing

        # DEBUG: check if region sizes match
        if verbose:
            print('[DEBUG: sizes of extracted regions:\n\t(0,0) = \t{}\n\t(-1,1) = \t{}\n\t(1,1) = \t{}\n\t(-1,-1) = \t{}\n\t(1,-1) = \t{}'.format(region_ctr_img.shape, region_tl_img.shape, region_tr_img.shape, region_bl_img.shape, region_br_img.shape))

        if self.__check_equal_regions__([region_ctr_img.shape, region_tl_img.shape, region_tr_img.shape, region_bl_img.shape, region_br_img.shape]) is False:
            print('[WARNING:] not all regions have the same size. Maybe increase tolerance or check alignment!')


        # calculate the modalities
        # absorption image
        abs_image = np.divide(np.fft.ifft2(region_ctr_img), np.fft.ifft2(region_ctr_ref))

        # dark-field images
        tl_image = np.divide(np.fft.ifft2(region_tl_img), np.fft.ifft2(region_tl_ref))
        tr_image = np.divide(np.fft.ifft2(region_tr_img), np.fft.ifft2(region_tr_ref))

        dfl_image = np.divide(abs(tl_image), abs(abs_image))
        dfr_image = np.divide(abs(tr_image), abs(abs_image))

        # phase-contrast images
        dpcl_image = self.__wrap_to_pi__(np.angle(np.fft.ifft2(region_tl_img)) - np.angle(np.fft.ifft2(region_tl_ref)))
        dpcr_image = self.__wrap_to_pi__(np.angle(np.fft.ifft2(region_tr_img)) - np.angle(np.fft.ifft2(region_tr_ref)))

        # linearise to sample thickness using the natural logarithm as described by Wen et al (2010)
        if correct_thickness is True:
            abs_image = abs(-np.log(abs_image))
            dfl_image = -np.log(dfl_image)
            dfr_image = -np.log(dfr_image)

        return abs_image, dfl_image, dfr_image, dpcl_image, dpcr_image
