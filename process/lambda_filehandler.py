#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 18 20:36:00 2020

@brief:  Handler for own HDF5 files obtained from the Lambda350K.

@author: Till, till.dreier@med.lu.se
"""

from pylambda.process.lambda_imageproc import ImageRegister, ImageInterpolate
from pylambda.process.lambda_dpc import ImageSingleGrating
from pylambda.process.lambda_phase import ImagePhase

import os
import tqdm
import numpy as np
from PIL import Image
# import hdf5plugin
import h5py
from scipy.signal import convolve2d
from scipy.ndimage import maximum_filter, median_filter
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm


class LambdaFiles(ImagePhase):
    """ """

    def __init__(self, datapath, datafile=None, flatpath=None, flatfile=None):
        """
        Work with self-created hdf5 files (copying EIGER data structure).
        Uses ImagePhase class (from lambda_phase) for phase-retrieval.
        :param datapath:        Path to the folder containing the data hdf5 files (master + n data files)
        :param datafile:        Name of the data master file or None to search for it in datapath (default).
        :param flatpath:        Path to the folder containing flatf-field data. Optional.
        :param flatfile:        Name of the flat master file or None to search for it in flatpath (default).
        """
        self.datapath = datapath
        self.datafile = datafile
        self.flatpath = flatpath
        self.flatfile = flatfile

        if self.datafile is None:
            self.datafile = self.__find_master__(self.datapath)
        if self.flatpath is not None and self.flatfile is None:
            self.flatfile = self.__find_master__(self.flatpath)

        # other stuff
        self.images_to_load = None
        self.vmasterfile = None

        # ROI
        self.x_roi_start = None
        self.y_roi_start = None
        self.x_roi_stop = None
        self.y_roi_stop = None

        # images
        self.flatfield = None
        self.images = None
        self.mask_detector = None
        self.mask_filter = None

        # phase-retrieval
        ImagePhase.__init__(self)


    def __find_master__(self, path, key="master"):
        """ find master file in PATH """
        mfile = None
        for file in os.listdir(path):
            if key in file:
                mfile = file
        return mfile


    def make_virtual_dataset(self, vdset_name="virtual.h5", mode="data"):
        """
        In case there is more than 1 data file, create a virtual dataset to browse the files as if they were a single file.
        :param data_master:     Provide name of the master file associated to multiple data files. If 'None', use the
                                file specified at object creation.
        :param vdset_name:      Name of the virtual master file (which has to be opened to read data)
        :param mode:            Select if working on data (default) or flat files.
        :return:                Total amount of images accessible through virtual master file.
        """

        if mode == "data":
            path = self.datapath
            fpath = os.path.join(path, self.datafile)
        elif mode == "flat":
            path = self.flatpath
            fpath = os.path.join(self.flatpath, self.flatfile)
        else:
            print('[ERROR:] invalid mode. Set either <data> or <flat>.')
            path = None
            fpath = None

        # make virtual dataset to access all data files as they were 1 (with continuous indices)
        with h5py.File(fpath, 'r') as f:
            dkeys = f["/entry/data"].keys()
            dsets = [dset for dset in dkeys]  # make list of keys (string)
            if len(dsets) > 1:
                print('[INFO:] more than 1 data file found!')
            else:
                print(
                    '[INFO:] only 1 dataset present, no virtual dataset needed.\nIf this is wrong, make sure the master file is associated with all data files.')
            # print('Data files: {}'.format(dsets))

            # get shape of virtual dataset
            shapes = [f["/entry/data/{}".format(dset)].shape for dset in dsets]
            max_idx = 0
            for s in shapes:
                max_idx += s[0]
                print(s)
            # print('Max idx: {}'.format(max_idx))
            dshape = (max_idx, shapes[0][1], shapes[0][2])

            # make virtual layout
            layout = h5py.VirtualLayout(dshape, dtype=np.uint32)
            m_start = 0

            for i, dataset in enumerate(dsets):
                # some debug information
                print('[INFO:] processing {}'.format(dataset))
                # idx_low = f["entry/data/{}".format(dataset)].attrs["image_nr_low"]
                # idx_high = f["entry/data/{}".format(dataset)].attrs["image_nr_high"]
                # print('IDX from {} to {}'.format(idx_low, idx_high))

                # fill layout
                dss = f["entry/data/{}".format(dataset)].shape
                m_end = m_start + dss[0]
                print('[INFO:] shape of dataset: {}, m_start = {}, m_end = {}'.format(dss, m_start, m_end))
                vsource = h5py.VirtualSource(f["entry/data/{}".format(dataset)])
                layout[m_start:m_end:1, :, :] = vsource
                m_start = m_end

                print('[INFO:] total amount of images to write into virtual data set: {}'.format(m_end))

            # write virtual dataset - to be used to access the data
            with h5py.File(os.path.join(path, vdset_name), 'w', libver='latest') as vf:
                vf.create_virtual_dataset('/entry/data/data_00000', layout, fillvalue=0)
                print("[INFO:] virtual dataset '{}' created in folder '{}'".format(vdset_name, self.datapath))

        return m_end  # total amount of images in file


    def use_virtual_dataset(self, key="virtual", mode="data"):
        """ Set the datafile or flatfile to the virtual master file (-> the file that contains KEY). """
        if mode == 'data':
            self.datafile = self.__find_master__(self.datapath, key=key)
            print('[INFO:] updated data_master to use virtual master file instead: {}'.format(self.datafile))
        elif mode == 'flat':
            self.flatfile = self.__find_master__(self.flatpath, key=key)
            print('[INFO:] updated flat_master to use virtual master file instead: {}'.format(self.datafile))
        else:
            print('[ERROR:] invalid mode for virtual dataset. Set <data> or <flat>.')


    def __remove_virtual_master__(self, virtual_file="virtual.h5", mode="data"):
        """
        Delete virtual master file.
        :param virtual_file:    Name of the virtual master file.
        :param mode:            Select data (default) or flat virtual master.
        """
        if "master" in virtual_file:
            print('[CRITICAL:] virtual master must not contain MASTER in file name!')
        else:
            if mode == 'data':
                if os.path.exists(os.path.join(self.datapath, virtual_file)):
                    os.remove(os.path.join(self.datapath, virtual_file))
                    print('[INFO:] removed file: {} (from folder: {})'.format(self.datafile, self.datapath))
                else:
                    print('[INFO:] virtual data master does not exist yet.')
            elif mode == 'flat':
                if os.path.exists(os.path.join(self.flatpath, virtual_file)):
                    os.remove(os.path.join(self.flatpath, virtual_file))
                    print('[INFO:] removed file: {} (from folder: {})'.format(self.flatfile, self.flatpath))
                else:
                    print('[INFO:] virtual flat master does not exist yet.')
            else:
                print('[ERROR:] invalid mode for virtual dataset. Set <data> or <flat>.')


    def set_images_to_load(self, images_to_load):
        """ provide a list of indices to load from file """
        if type(images_to_load) in [tuple, list]:
            self.images_to_load = images_to_load
        else:
            self.images_to_load = list(images_to_load)


    def reset_images_to_load(self):
        """ Reset self.images_to_load so when calling self.load_images() all images are loaded. """
        self.images_to_load = None


    def set_roi(self, x_start, y_start, x_stop, y_stop):
        """
        Define a ROI of the data to use.
        :param x_start:     Start coordinate in x.
        :param y_start:     Start coordinate in y.
        :param x_stop:      End coordinate in x.
        :param y_stop:      End coordinate in y.
        """
        self.x_roi_start = x_start
        self.x_roi_stop = x_stop
        self.y_roi_start = y_start
        self.y_roi_stop = y_stop
        print('[INFO:] ROI selection set: from ({},{}) to ({},{}).'.format(self.x_roi_start, self.y_roi_start, self.x_roi_stop, self.y_roi_stop))


    def reset_roi(self):
        """
        Remove the ROI selection.
        """
        self.x_roi_start = None
        self.x_roi_stop = None
        self.y_roi_start = None
        self.y_roi_stop = None
        print('[INFO:] ROI selection removed')


    def load_detector_mask(self, path, fname):
        """
        Load a mask_detector array (8 bit) and convert to boolean mask_detector.
        :param path:    Path to folder containing mask_detector.
        :param fname:   File name of the mask_detector tiff file
        """
        self.mask_detector = np.array(Image.open(os.path.join(path, fname))).astype(bool)
        self.mask_detector = self.mask_detector[self.x_roi_start:self.x_roi_stop, self.y_roi_start:self.y_roi_stop]


    def save_to_tiff(self, image, path, fname, to_uint16=True, global_min=None, global_max=None):
        """ Save an image (numpy array) to TIFF, convert to uint16 (default). """
        if to_uint16: image = self.__image_to_uint16__(image, global_min, global_max)
        if not os.path.isdir(path): os.mkdir(path)
        if ".tiff" not in fname: fname += ".tiff"
        tmp = Image.fromarray(image)
        tmp.save(os.path.join(path, fname))


    def save_all_images_to_tiff(self, path, prefix, to_uint16=True, global_minmax=False):
        """
        Write all images in self.image to PATH and name them 'prefix_000x.tiff.
        Set to_uint16 to True to convert the images to uint16.
        Set global_minmax to True to use the global minimum and maximum of the whole stack when converting to uint16.
        """
        print('[INFO:] writing {} images to tiff into: {}'.format(self.images.shape[0], path))
        if global_minmax is True:
            g_min = np.amin(self.images)
            g_max = np.amax(self.images)
            print('[INFO:] normlising images to global minimum to {} and global maximum to {}'.format(g_min, g_max))
            self.normalise_images(global_minmax)
        else:
            g_min = None
            g_max = None
        for idx, image in enumerate(self.images):
            fname = "{}_{:04}.tiff".format(prefix, idx)
            self.save_to_tiff(image, path, fname, to_uint16, g_min, g_max)


    def __image_to_uint16__(self, image, global_min=None, global_max=None):
        """
        Convert a provided image to uint16.
        :param image:       Image (numpy array)
        :param global_max:  Force global maximum
        :return:            Input image converted to 16-bit unsigned integer.
        """
        # global or local (current image) maximum
        if global_max is not None:
            img_max = global_max
        else:
            img_max = np.amax(image)

        # global or local (current image) minimum
        if global_min is not None:
            img_min = global_min
        else:
            img_min = np.amin(image)

        # handle negative values
        if img_min < 0:
            image = np.add(image, abs(img_min))

        return np.multiply(np.divide(image, img_max), 2 ** 16 - 1).astype(np.uint16)


    def load_flatfield(self, remove_zeros=False, indices=None):
        """ 
        Load flatfield from file.
        Provide a list of indicies to load specific flats.
        """        
        with h5py.File(os.path.join(self.flatpath, self.flatfile), "r") as f:
            dset = f["/entry/data/data_00000"]
            if indices is None:
                print('[INFO:] loading all flats ...')
                self.flatfield = np.average(dset[:, self.x_roi_start:self.x_roi_stop, self.y_roi_start:self.y_roi_stop], axis=0)
            else:
                print('[INFO:] loading flats using provided indices (list of {}) ...'.format(len(indices)))
                self.flatfield = np.average(dset[indices, self.x_roi_start:self.x_roi_stop, self.y_roi_start:self.y_roi_stop], axis=0)
        # remove zeros
        if remove_zeros is True:
            self.flatfield[self.flatfield == 0] = np.mean(self.flatfield)


    def flat_from_tiff(self, fpath):
        """ Load a flat from tiff file. Mostly for testing. """
        self.flatfield = np.array(Image.open(fpath))
        self.flatfield = self.flatfield[self.x_roi_start:self.x_roi_stop, self.y_roi_start:self.y_roi_stop]


    def apply_flatfield(self):
        """ Apply flatfield correction to self.images """
        self.images = np.divide(self.images, self.flatfield)
        print('[INFO:] flat-field correction applied.')


    def load_images(self):
        """ load images """
        print('[INFO:] loading images ... ')
        with h5py.File(os.path.join(self.datapath, self.datafile), "r") as f:
            dset = f["/entry/data/data_00000"]
            if self.images_to_load is None:
                self.images = dset[:, self.x_roi_start:self.x_roi_stop, self.y_roi_start:self.y_roi_stop]
            else:
                self.images = dset[self.images_to_load, self.x_roi_start:self.x_roi_stop, self.y_roi_start:self.y_roi_stop]
        print('[INFO:] {} images loaded'.format(self.images.shape[0]))


    def get_preview_image(self, index=0, use_roi=True):
        """ Load 1 image from file. """
        with h5py.File(os.path.join(self.datapath, self.datafile), "r") as f:
            dset = f["/entry/data/data_00000"]
            if use_roi is True:
                img = dset[index, self.x_roi_start:self.x_roi_stop, self.y_roi_start:self.y_roi_stop]
            else:
                img = dset[index]
        return img


    def preview_roi(self, image_idx=0, vmax=None):
        """
        Load image from HDF5 file and apply ROI settings.
        :param image_idx:   Index of the image to use.
        """
        with h5py.File(os.path.join(self.datapath, self.datafile), "r") as f:
            # create dataset to access images (does not load data into RAM)
            tmp = f["/entry/data/data_00000"]

            # load image into memory
            preview_image = tmp[image_idx, self.x_roi_start:self.x_roi_stop, self.y_roi_start:self.y_roi_stop]

        fig = plt.figure()
        plt.imshow(preview_image, norm=LogNorm(), vmax=vmax)
        plt.title("Preview of ROI selection using image at index {}".format(image_idx))
        plt.show()


    def filter_mask(self, outliers=True, dead=True, noisy=True,
                    filter_size=5, outlier_thl=2, outlier_maxpix=500,
                    dead_thl=0.1, dead_maxpix=500,
                    noisy_thl=1.3, noisy_maxpix=200,
                    apply_mask=True,
                    verbose=False):
        """
        Create a mask (3D array, same shape as self.images) to mask outliers, noisy, and dead pixels.
        Works on self.images after flat-field correction.
        To only create the mask, run with apply_mask = False.
        To only apply the mask, run with apply_mask = True and outliers, dead, noisy = False.
        It's faster to apply the mask immediately.
        :param outliers:            When True, mask outliers.
        :param dead:                When True, mask dead pixels.
        :param noisy:               When True, mask noisy pixels (over certain thl).
        :param filter_size:         Size of the median filter used to fill in missing values.
        :param outlier_thl:         Value used to find hot pixels (outliers): thl * std(diff_img)
        :param outlier_maxpix:      Maximum amount of pixels per frame to mask.
        :param dead_thl:            Threshow below which pixel value to consider a pixel dead:
                                    typically 0.1 or 0.2 work on normalised images.
        :param dead_maxpix:         Maximum amount of pixels per frame to mask.
        :param noisy_thl:           Threshold above which to consider a pixel noisy:
                                    typically 1.3 catches most of them keeping phase fringes.
        :param noisy_maxpix:        Maximum amount of pixels per frame to mask.
        :param apply_mask:          Apply the created mask to self.images
        :param verbose:             Print status and debug info when True
        """
        if self.images.dtype not in [float, np.float32, np.float64]:
            print('[ERROR:] images are not of type float. Flat-field correction not applied?')

        self.mask_filter = np.zeros(self.images.shape).astype(bool)

        for idx, img in enumerate(tqdm.tqdm(self.images)):

            if verbose:
                print('Creating mask for image {}/{} ...'.format(idx + 1, self.images.shape[0]))

            # prepare filtered image
            img[np.isnan(img)] = np.inf
            filt = median_filter(img, filter_size, mode='wrap')

            # mask outliers/hot pixels
            if outliers is True:

                # difference image
                diff = np.subtract(img, filt)
                diff[np.isnan(diff)] = np.inf  # remove NaNs that might occur

                # standard deviation (ignoring inf)
                std_grey = np.std(diff[~np.isinf(diff)])  # standard deviation

                # mask_detector hot pixels
                mask = abs(diff) > (outlier_thl * std_grey)
                masked = len(np.where(mask >= 1)[0])

                if verbose:
                    print('\t{} outliers in image at idx {}'.format(masked, idx))

                if masked > outlier_maxpix:
                    print('\tMore than {} outliers in image at idx = {}. Skipping ...'.format(masked, idx))
                else:
                    self.mask_filter[idx] |= mask

            # mask dead pixels
            if dead is True:
                mask = img <= dead_thl
                mask = mask.astype(bool)
                masked = len(np.where(mask == True)[0])

                if verbose:
                    print('\t{} dead pixels in image at idx {}'.format(masked, idx))

                if masked > dead_maxpix:
                    print('\tMore than {} dead pixels in image at idx = {}. Skipping ...'.format(masked, idx))
                else:
                    self.mask_filter[idx] |= mask

            # mask noisy pixels
            if noisy is True:
                mask = img > noisy_thl
                mask = mask.astype(bool)
                masked = len(np.where(mask is True)[0])

                if verbose:
                    print('\t{} noisy pixels in image at idx {}'.format(masked, idx))

                if masked > noisy_maxpix:
                    print('\tMore than {} noisy pixels in image at idx = {}. Skipping ...'.format(masked, idx))
                else:
                    self.mask_filter[idx] |= mask

            # apply mask to images
            if apply_mask is True:
                corr = img.copy()
                corr[self.mask_filter[idx]] = filt[self.mask_filter[idx]]
                self.images[idx] = corr


    def filter_mask_flats(self, outliers=True, dead=True, noisy=True,
                    filter_size=5, outlier_thl=2, outlier_maxpix=500,
                    dead_thl=0.1, dead_maxpix=500,
                    noisy_thl=1.3, noisy_maxpix=200,
                    apply_mask=True,
                    verbose=False):
        """
        When using 1 flat image per position in DPC CT imaging.
        Create a mask (3D array, same shape as self.flatfield) to mask outliers, noisy, and dead pixels.
        Works on self.flatfield.
        To only create the mask, run with apply_mask = False.
        To only apply the mask, run with apply_mask = True and outliers, dead, noisy = False.
        It's faster to apply the mask immediately.
        :param outliers:            When True, mask outliers.
        :param dead:                When True, mask dead pixels.
        :param noisy:               When True, mask noisy pixels (over certain thl).
        :param filter_size:         Size of the median filter used to fill in missing values.
        :param outlier_thl:         Value used to find hot pixels (outliers): thl * std(diff_img)
        :param outlier_maxpix:      Maximum amount of pixels per frame to mask.
        :param dead_thl:            Threshow below which pixel value to consider a pixel dead:
                                    typically 0.1 or 0.2 work on normalised images.
        :param dead_maxpix:         Maximum amount of pixels per frame to mask.
        :param noisy_thl:           Threshold above which to consider a pixel noisy:
                                    typically 1.3 catches most of them keeping phase fringes.
        :param noisy_maxpix:        Maximum amount of pixels per frame to mask.
        :param apply_mask:          Apply the created mask to self.images
        :param verbose:             Print status and debug info when True
        """
        if self.flatfield.dtype not in [float, np.float32, np.float64]:
            print('[ERROR:] images are not of type float. Flat-field correction not applied?')

        self.mask_filter = np.zeros(self.flatfield.shape).astype(bool)

        for idx, img in enumerate(tqdm.tqdm(self.flatfield)):

            if verbose:
                print('Creating mask for flat {}/{} ...'.format(idx + 1, self.flatfield.shape[0]))

            # prepare filtered image
            img[np.isnan(img)] = np.inf
            filt = median_filter(img, filter_size, mode='wrap')

            # mask outliers/hot pixels
            if outliers is True:

                # difference image
                diff = np.subtract(img, filt)
                diff[np.isnan(diff)] = np.inf  # remove NaNs that might occur

                # standard deviation (ignoring inf)
                std_grey = np.std(diff[~np.isinf(diff)])  # standard deviation

                # mask_detector hot pixels
                mask = abs(diff) > (outlier_thl * std_grey)
                masked = len(np.where(mask >= 1)[0])

                if verbose:
                    print('\t{} outliers in flatfield at idx {}'.format(masked, idx))

                if masked > outlier_maxpix:
                    print('\tMore than {} outliers in flatfield at idx = {}. Skipping ...'.format(masked, idx))
                else:
                    self.mask_filter[idx] |= mask

            # mask dead pixels
            if dead is True:
                mask = img <= dead_thl
                mask = mask.astype(bool)
                masked = len(np.where(mask == True)[0])

                if verbose:
                    print('\t{} dead pixels in flatfield at idx {}'.format(masked, idx))

                if masked > dead_maxpix:
                    print('\tMore than {} dead pixels in flatfield at idx = {}. Skipping ...'.format(masked, idx))
                else:
                    self.mask_filter[idx] |= mask

            # mask noisy pixels
            if noisy is True:
                mask = img > noisy_thl
                mask = mask.astype(bool)
                masked = len(np.where(mask is True)[0])

                if verbose:
                    print('\t{} noisy pixels in flatfield at idx {}'.format(masked, idx))

                if masked > noisy_maxpix:
                    print('\tMore than {} noisy pixels in flatfield at idx = {}. Skipping ...'.format(masked, idx))
                else:
                    self.mask_filter[idx] |= mask

            # apply mask to images
            if apply_mask is True:
                corr = img.copy()
                corr[self.mask_filter[idx]] = filt[self.mask_filter[idx]]
                self.flatfield[idx] = corr


    def edit_filter_mask(self, index, new_mask):
        """
        Edit self.mask_filter at a specific index.
        :param index:       Index at which to edit the mask.
        :param new_mask:    New mask (2D boolean numpy array).
        """
        if new_mask.dtype is not bool:
            print('[CRITICAL:] mask needs to be boolean.')

        self.mask_filter[index] = new_mask


    def apply_filter_mask_to_index(self, index, filter_size=5, verbose=False):
        """
        Apply self.mask_filter to self.images at a specific index.
        :param index:           Index of image/mask.
        :param filter_size:     Size of the median filter used to fill in masked values.
        :param verbose:         When True, print debug info.
        """
        if self.images.dtype not in [float, np.float32, np.float64]:
            print('[ERROR:] images are not of type float. Flat-field correction not applied?')

        # prepare filtered image
        img = self.images[index]
        img[np.isnan(img)] = np.inf
        filt = median_filter(img, filter_size, mode='wrap')

        # apply mask
        if verbose:
            print('[DEBUG:] applying mask to image at index {}'.format(index))
        masked = img.copy()
        masked[self.mask_filter[index]] = filt[self.mask_filter[index]]


    def remove_outliers(self, image, size=5, thl=2, max_pixels=500, verbose=False):
        """
        Remove hot pixels from image. Filling in values via a median filter.
        :param image:           Input image as 2D numpy array. Will be converted to float32 if necessary.
        :param size:            Size of the kernel used for median filter.
        :param thl:             Threshold for outlier detection (thl * std_dev).
        :param max_pixels:      Do not replace any pixels when more than this amount would be replaced.
        :param verbose:         When True, print the number of masked pixels and the standard deviation.
        :return:                Corrected image as 2D numpy array.
        """
        # convert to float if necessary
        if image.dtype not in [float, np.float32, np.float64]:
            image = image.astype(np.float32)

        # remove NaN
        image[np.isnan(image)] = np.inf

        # filter image
        filt = median_filter(image, size, mode='wrap')

        # difference image
        diff = np.subtract(image, filt)
        diff[np.isnan(diff)] = np.inf  # remove NaNs that might occur

        # standard deviation (ignoring inf)
        std_grey = np.std(diff[~np.isinf(diff)])  # standard deviation

        # mask_detector hot pixels
        mask = abs(diff) > (thl * std_grey)
        masked = len(np.where(mask >= 1)[0])

        if verbose:
            print('STD: {}'.format(std_grey))
            print('Hot pixels: {}'.format(masked))

        # throw warning when too many pixels found
        if masked > max_pixels:
            print('[WARNING:] image has too many pixels (>{}) to replace, skipping ...'.format(masked))
            return image

        else:

            # replace masked pixels
            corr = image.copy()
            corr[mask] = filt[mask]
            return corr


    def remove_outliers_from_all_images(self, size=5, thl=2, max_pixels=500, verbose=False):
        """
        Find and remove outliers from all self.images.
        :param size:            Size of the kernel for the median filter.
        :param thl:             Threshold for outlier detection (thl * std_dev). 2 works well with extremely noisy pixels.
        :param max_pixels:      Do not replace any pixels when more than this amount would be replaced.
        :param verbose:         When True, print the number of masked pixels and the standard deviation.
        """
        for idx, img in enumerate(tqdm.tqdm(self.images)):
            self.images[idx] = self.remove_outliers(img, size, thl, max_pixels, verbose)


    def remove_masked(self, image, filter_size=10, mask=None, verbose=False):
        """
        Remove masked pixels from image. Filling in values via a median filter.
        :param image:           Input image as 2D numpy array.
        :param filter_size:     Size of the kernel for the median filter (10 works well on XL pixels of Lambda detector).
        :param verbose:         When True, print the number of masked pixels.
        :return:                Filtered image as 2D numpy array.
        """
        if image.dtype not in [float, np.float32, np.float64]:
            image = image.astype(np.float32)

        if mask is None:
            mask = self.mask_detector

        # remove NaN
        image[np.isnan(image)] = np.inf

        # apply median filter
        filt = median_filter(image, filter_size)

        # replace masked values with values from filtered image
        if verbose:
            print('[DEBUG:] Masked pixels: {}'.format(len(np.where(mask >= 1)[0])))
        corr = image.copy()
        corr[mask] = filt[mask]
        return corr


    def remove_masked_from_all_images(self, filter_size=10, verbose=False):
        """
        Remove pixels masked in self.mask_detector from self.images.
        :param filter_size:     Size of the kernel for the median filter (10 works well for Lambda XL pixels).
        :param verbose:         When True, print the number of masked pixels and the standard deviation.
        """
        for idx, img in enumerate(tqdm.tqdm(self.images)):
            self.images[idx] = self.remove_masked(img, filter_size, mask=None, verbose=verbose)


    def remove_masked_from_all_flatfields(self, filter_size=10, verbose=False):
        """
        Remove pixels masked in self.mask_detector from self.flatfield.
        :param filter_size:     Size of the kernel for the median filter (10 works well for Lambda XL pixels).
        :param verbose:         When True, print the number of masked pixels and the standard deviation.
        """
        for idx, img in enumerate(tqdm.tqdm(self.flatfield)):
            self.flatfield[idx] = self.remove_masked(img, filter_size, mask=None, verbose=verbose)


    def remove_pixels_above_value(self, image, value=1.3, filter_size=5, verbose=False):
        """
        Remove pixels from image above value. Clean up images after removing outliers and mask_detector.
        :param image:       Image to remove pixels from as 2D numpy array.
        :param value:       Value above which to mask_detector a pixel. 1.3 works well after proper flat-field correction.
        :param verbose:     When True print the amount of pixels being removed.
        :return:            Cleaned up image as 2D numpy array.
        """
        # FIXME: add option to set maximum amount of pixels to filter out.
        mask = image > value
        mask = mask.astype(bool)
        # masked = len(np.where(mask_detector is True)[0])

        filtered = self.remove_masked(image, filter_size, mask, verbose)
        return filtered


    def remove_pixels_above_value_from_all_images(self, value=1.3, filter_size=5, verbose=False):
        """
        Remove pixels above a certain value from self.images. Call this after removing outliers and mask_detector to clean up
        any pixels that might have been left out.
        :param value:           Pixel value above which to mask_detector a pixel.
                                With proper flat-field correction 1.3 should work fine, keeping phase fringes.
        :param filter_size:     Size of the kernel used in median filter.
        :param verbose:         When True print the amount of pixels that are being removed
        """
        for idx, img in enumerate(tqdm.tqdm(self.images)):
            self.images[idx] = self.remove_pixels_above_value(img, value, filter_size, verbose)


    def remove_dead_pixels(self, image, value=0.1, filter_size=5, max_pixels=500, verbose=False):
        """
        Remove pixels from image below value. Dead pixels are not necessarily always 0.
        :param image:           Image to remove pixels from as 2D numpy array.
        :param value:           Value below which to consider pixels dead. 0.2 works well after proper flat-field correction.
        :param filter_size:     Size of the kernel for the median filter.
        :param max_pixels:      Maximum amount of pixels to replace.
        :param verbose:         When True print the amount of dead pixels being removed.
        """

        # remove NaN
        image[np.isnan(image)] = np.inf

        mask = image <= value
        mask = mask.astype(bool)
        masked = len(np.where(mask == True)[0])

        if verbose:
            print('[DEBUG:] {} dead pixels'.format(masked))

        if masked > max_pixels:
            print('[WARNING:] image has too many pixels (>{}) to replace, skipping ...'.format(masked))
            return image

        else:
            # apply median filter
            filt = median_filter(image, filter_size)

            corr = image.copy()
            corr[mask] = filt[mask]
            return corr


    def remove_dead_pixels_from_all_images(self, value=0.1, filter_size=5, max_pixels=500, verbose=False):
        """

        :param value:
        :param filter_size:
        :param verbose:
        :return:
        """
        for idx, img in enumerate(tqdm.tqdm(self.images)):
            self.images[idx] = self.remove_dead_pixels(img, value, filter_size, max_pixels, verbose)


    def normalise_images(self, global_minmax=False):
        """ normalise self.images """
        if global_minmax is True:
            g_min = np.amin(self.images)
            g_max = np.amax(self.images)
            print('[INFO:] setting global minimum to {} and global maximum to {}'.format(g_min, g_max))
        else:
            g_min = None
            g_max = None
        for idx, img in enumerate(self.images):
            self.images[idx] = (img - g_min) / (g_max - g_min)




class LambdaSRFiles(LambdaFiles, ImageRegister, ImageInterpolate, ImagePhase):
    """ """

    def __init__(self, datapath, datafile=None, flatpath=None, flatfile=None):
        """
        Class to handle Lambda scans with shifted low-res images that are to be interpolated and combined.
        Uses LambdaFiles for file access and preparation (flat-field correction, filtering noisy pixels, etc.)
        Uses ImageRegister to register shifts between low-res images.
        Uses ImageInterpolate for interpolation and combination of images.
        Uses ImagePhase for phase-retrieval.
        :param datapath:    Path to the folder containing data master file.
        :param datafile:    Name of the data master file or None to search datapath for a file with "master" in its name.
        :param flatpath:    Path to the folder containing the flat master file. Can be None when no flat-field data is available.
        :param flatfile:    Name of the flat master file or None to search flatpath for a file with "master" in its name.
        """
        LambdaFiles.__init__(self, datapath, datafile, flatpath, flatfile)
        ImageRegister.__init__(self)
        ImageInterpolate.__init__(self)
        ImagePhase.__init__(self)


    def load_position_flatfields(self, flats_per_position):
        """ """
        with h5py.File(os.path.join(self.flatpath, self.flatfile), "r") as f:
            dset = f["/entry/data/data_00000"]
            print('[INFO:] loading flats ...')
            tmp = dset[:, self.x_roi_start:self.x_roi_stop, self.y_roi_start:self.y_roi_stop]

        positions = int(tmp.shape[0] / flats_per_position)
        print('[INFO:] Extracting {} flat-fields per position, for {} positions.'.format(flats_per_position, positions))

        # allocate space
        self.flatfield = np.zeros((positions, tmp.shape[1], tmp.shape[2]))

        start = 0
        stop = flats_per_position
        for idx in range(positions):
            self.flatfield[idx] = np.average(tmp[start:stop], axis=0)
            start += flats_per_position
            stop += flats_per_position


    def apply_flatfield_with_position_correction(self, correction_factors, verbose=False):
        """
        When loading 1 projections consisting of X low-res images,
        apply flatfield correction with a correction factor to account for intensity variations.
        The flatfield for a position is multiplied with the given factor.
        :param correction_factors:  List or tuple of factors. 1 Factor per image. 1 means no change.
        :param verbose:             When True, print debug information.
        """
        if type(correction_factors) not in [list, tuple]:
            print('[ERROR:] correction factors have to be a tuple or a list, not {}'.format(type(correction_factors)))

        else:

            if len(correction_factors) != self.images.shape[0]:
                print('[ERROR:] number of correction factors does not match number of images: {} vs {}'.format(len(correction_factors), self.images.shape[0]))

            else:
                self.images = self.images.astype(float)  # force conversion to float
                for idx, (img, fac) in enumerate(zip(self.images, correction_factors)):
                    self.images[idx] /= self.flatfield * fac
                    if verbose is True:
                        print('[DEBUG:] correcting flatfield at index {} with factor {}'.format(idx, fac))




class LambdaDPCFiles(LambdaFiles, ImageSingleGrating):
    """ """

    # FIXME: update ImageSingleGrating to use class variables
    def __init__(self, datapath, datafile=None, flatpath=None, flatfile=None, trueflatpath=None, trueflatfile=None):
        """
        Class to handle Lambda images obtained with a single absorption grating.
        Adds a TrueFlat: (x-rays on, no sample, no grating). The Flat-field should contain the grating.
        :param datapath:        Path to the folder containing data master file.
        :param datafile:        Name of the data master file or None to search datapath for a file with "master" in its name.
        :param flatpath:        Path to the folder containing the flat master file. Can be None when no flat-field data is available.
        :param flatfile:        Name of the flat master file or None to search flatpath for a file with "master" in its name.
        :param trueflatpath:    Path to the folder containing true flat master file. Can be None when no true flat-field is available.
        :param trueflatfile:    Name of the true flat master file or None to search trueflatpath for a file with "master" in its name.
        """
        LambdaFiles.__init__(self, datapath, datafile, flatpath, flatfile)
        ImageSingleGrating.__init__(self)
        self.trueflatpath = trueflatpath
        self.trueflatfile = trueflatfile
        self.trueflat = None
        self.proj_abs = None
        self.proj_dfl = None
        self.proj_dfr = None
        self.proj_dpcl = None
        self.proj_dpcr = None

        if self.trueflatpath is not None and self.trueflatfile is None:
            self.trueflatfile = self.__find_master__(self.trueflatpath)


    def load_trueflat(self):
        """ """
        with h5py.File(os.path.join(self.trueflatpath, self.trueflatfile), "r") as f:
            dset = f["/entry/data/data_00000"]
            self.trueflat = np.average(dset[:, self.x_roi_start:self.x_roi_stop, self.y_roi_start:self.y_roi_stop], axis=0)


    def apply_trueflat(self, mode='image'):
        """ Correct self.images or self.flatfield with self.trueflat """
        if mode == "image":
            self.images = np.divide(self.images, self.trueflat)
        elif mode == "flat":
            self.flatfield = np.divide(self.flatfield, self.trueflat)
        else:
            print("[ERROR:] invalid mode for true-flat correction. Select 'image' or 'flat'.")


    def load_all_flatfields(self, remove_zeros=False, indices=None):
        """
        Load all flat-field images from file.
        Provide a list of indicies to load specific flats.
        Use this when loading individual flats per position.
        """
        with h5py.File(os.path.join(self.flatpath, self.flatfile), "r") as f:
            dset = f["/entry/data/data_00000"]
            if indices is None:
                print('[INFO:] loading all flats ...')
                self.flatfield = dset[:, self.x_roi_start:self.x_roi_stop, self.y_roi_start:self.y_roi_stop]
            else:
                print('[INFO:] loading flats using provided indices (list of {}) ...'.format(len(indices)))
                self.flatfield = dset[indices, self.x_roi_start:self.x_roi_stop, self.y_roi_start:self.y_roi_stop]
        # remove zeros
        if remove_zeros is True:
            self.flatfield[self.flatfield == 0] = np.mean(self.flatfield)


    def extract_modalities_all_projections(self, absorption=True, darkfield=True, phasecontrast=True,
                                           pixels_per_period=4, tolerance=2, min_distance='auto',
                                           correct_thickness=True, iteratively=False, verbose=False,
                                           savepath_base=None, flat_per_position=False):
        """
        Extract absorption, dark-field (left+right) and differential phase-contrast (left+right) from loaded images.
        :param absorption:              When True, extract absorption images.
        :param darkfield:               When True, extract dark-field images.
        :param phasecontrast:           When True, extarct differential phase-contrast images.
        :param pixels_per_period:       Alignment of the grating. So far only 3 and 4 are implemented.
        :param tolerance:               Amount of pixels by which to reduce area size.
                                        Increase tolerance if the region sizes are not equal.
        :param min_distance:            Minimum distance to the next maximum in Fourier space, set 'auto' to use a
                                        working value based on pixels_per_period.
        :param correct_thickness:       When True, apply -ln to the resulting modalities.
        :param iteratively:             When True, process 1 image at a time and save it to file.
                                        When False, process all images and keep them in memory.
                                        Results have to be saved by calling save_modalities().
        :param verbose:                 When True, print debug information.
        :param savepath_base:           Base path in which to create sub-folders for the modalities.
        :param flat_per_position:       When True, self.flatfield contains 1 flat corresponding to the image at the same
                                        index in self.images.
        :return:
        """
        for idx, img in enumerate(tqdm.tqdm(self.images)):
            # create modalities
            if flat_per_position is False:
                absorp, dfl, dfr, dpcl, dpcr = self.extract_modalities(img, self.flatfield, pixels_per_period,
                                                                       correct_thickness, verbose, tolerance, min_distance)
            else:
                absorp, dfl, dfr, dpcl, dpcr = self.extract_modalities(img, self.flatfield[idx], pixels_per_period,
                                                                       correct_thickness, verbose, tolerance, min_distance)

            # save to file, don't keep in memory
            if iteratively is True:
                if absorption is True:
                    self.save_to_tiff(absorp, os.path.join(savepath_base, "Proj_absorption"), "proj_abs_{:04}.tiff".format(idx), to_uint16=False)
                if darkfield is True:
                    self.save_to_tiff(dfl, os.path.join(savepath_base, "Proj_dfl"), "proj_dfl_{:04}.tiff".format(idx), to_uint16=False)
                    self.save_to_tiff(dfl, os.path.join(savepath_base, "Proj_dfr"), "proj_dfr_{:04}.tiff".format(idx), to_uint16=False)
                if phasecontrast is True:
                    self.save_to_tiff(dpcl, os.path.join(savepath_base, "Proj_dpcl"), "proj_dpcl_{:04}.tiff".format(idx), to_uint16=False)
                    self.save_to_tiff(dpcl, os.path.join(savepath_base, "Proj_dpcr"), "proj_dpcr_{:04}.tiff".format(idx), to_uint16=False)

            # keep files in memory
            else:

                if idx == 0:
                    # allocate space
                    num_files = self.images.shape[0]
                    xregion, yregion = absorp.shape
                    if absorption is True:
                        self.proj_abs = np.zeros((num_files, xregion, yregion))
                    if darkfield is True:
                        self.proj_dfl = np.zeros((num_files, xregion, yregion))
                        self.proj_dfr = np.zeros((num_files, xregion, yregion))
                    if phasecontrast is True:
                        self.proj_dpcl = np.zeros((num_files, xregion, yregion))
                        self.proj_dpcr = np.zeros((num_files, xregion, yregion))

                # FIXME: add padding if region sizes are not equal

                if absorption is True:
                    self.proj_abs[idx] = absorp
                if darkfield is True:
                    self.proj_dfl[idx] = dfl
                    self.proj_dfr[idx] = dfr
                if phasecontrast is True:
                    self.proj_dpcl[idx] = dpcl
                    self.proj_dpcr[idx] = dpcr


    def save_modalities(self, savefolder_base):
        """
        Save the extracted modalities.
        :param savefolder_base:     Base folder, will create sub-folders for each modality.
        :return:
        """
        spath_abs = os.path.join(savefolder_base, "Proj_abs")
        spath_dfl = os.path.join(savefolder_base, "Proj_dfl")
        spath_dfr = os.path.join(savefolder_base, "Proj_dfr")
        spath_dpcl = os.path.join(savefolder_base, "Proj_dpcl")
        spath_dpcr = os.path.join(savefolder_base, "Proj_dpcr")

        if self.proj_abs is not None:
            print('[INFO:] saving absorption images to tiff ...')
            for idx, img in enumerate(self.proj_abs):
                self.save_to_tiff(img, spath_abs, "proj_abs_{:04}.tiff".format(idx), to_uint16=False)

        if self.proj_dfr is not None and self.proj_dfl is not None:
            print('[INFO:] saving dark-field images to tiff ...')
            for idx, (dfl, dfr) in enumerate(zip(self.proj_dfl, self.proj_dfr)):
                self.save_to_tiff(dfl, spath_dfl, "proj_dfl_{:04}.tiff".format(idx), to_uint16=False)
                self.save_to_tiff(dfr, spath_dfr, "proj_dfr_{:04}.tiff".format(idx), to_uint16=False)

        if self.proj_dpcr is not None and self.proj_dpcl is not None:
            print('[INFO:] saving phase-contrast images to tiff ...')
            for idx, (dpcl, dpcr) in enumerate(zip(self.proj_dpcl, self.proj_dpcr)):
                self.save_to_tiff(dpcl, spath_dpcl, "proj_dpcl_{:04}.tiff".format(idx), to_uint16=False)
                self.save_to_tiff(dpcr, spath_dpcr, "proj_dpcr_{:04}.tiff".format(idx), to_uint16=False)



