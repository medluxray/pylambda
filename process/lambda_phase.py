#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 24 09:18:00 2020

@brief:  Phase-retrieval for Lambda data.
         Using Ulf's approach.

@author: Till, till.dreier@med.lu.se
"""

import os
import math
import numpy as np
from PIL import Image
import scipy.constants as const


class ImagePhase:
    """ """

    def __init__(self):
        """
        Perform Paganin phase-retrieval on a provided image.
        Usage:
            1. Set the Geometry:               set_geometry()
            2. Set guess delta/beta:           config_phaseretrieval()
            3. Calculate frequency magnitude:  calculate_frequency_magnitude()
            4. Run phase-retrieval:            paganin()

        Vary delta/beta (step 2) until phase-fringes around object disappear.

        With CT data, run 1-3 until reasonable guess for delta/beta, then loop over 4 for all images.
        """
        # Geometry
        self.dist_source_detector = None
        self.dist_source_object = None
        self.pixelsize_um = None

        # Effective Geometry
        self.effective_pixelsize_x = None
        self.effective_pixelsize_y = None
        self.magnification = None
        self.propagation_distance = None

        # phase-retrieval params
        self.energy_eV = None
        self.delta = None
        self.beta = None
        self.lmbda = None
        self.mu = None
        self.delta_over_mu = None
        self.alpha = None

        # frequency matrices
        self.w2 = None


    def load_tiff(self, path, fname):
        """ """
        return np.array(Image.open(os.path.join(path, fname)))


    def save_tiff(self, array, path, fname):
        """ """
        tmp = Image.fromarray(array)
        if ".tiff" not in fname: fname += ".tiff"
        tmp.save(os.path.join(path, fname))


    def __deconvolution__(self, img, h):
        """
        Deconvolve a provided image with a filter.
        :param img:     Image in real space as 2D numpy array.
        :param h:       Filter. E.g. created as part of paganin().
        :return:        Deconvolved image as 2D numpy array.
        """
        decon = np.fft.ifft2(np.fft.ifftshift(np.fft.fftshift(np.fft.fft2(img)) * (1 / h)))
        return decon


    def set_geometry(self, source_detector, source_object, pixel_size_um):
        """
        Set the measurement geometry, calculate magnification, effective pixels size and propagation distance.
        :param source_detector:         Distance from source to detector in m.
        :param source_object:           Distance from source to object in m.
        :param pixel_size_um:           Pixel size in um.
        """
        self.dist_source_detector = source_detector
        self.dist_source_object = source_object
        self.pixelsize_um = pixel_size_um

        self.magnification = self.dist_source_detector / self.dist_source_object  # magnification factor
        self.effective_pixelsize_x = self.pixelsize_um / self.magnification  # effective pixel size in x
        self.effective_pixelsize_y = self.pixelsize_um / self.magnification  # effective pixel size in y
        self.propagation_distance = (self.dist_source_detector - self.dist_source_object) / self.magnification  # effective propagation distance in m ((SD - SO) / M)


    def config_phaseretrieval(self, delta=3.7e-8, beta=1.7e-10, energy_ev=8390):
        """
        Guess delta/beta parameters and calulate remaining parameters for phase-retrieval.
        :param delta:       refractive index decrement of the object.
        :param beta:        complex part of the refractive index of the object.
        :param energy_ev:   Beam energy in eV. With polychromatic source use distinct peaks or average thereof.
        """
        self.delta = delta  # refractive index decrement of the object
        self.beta = beta  # 3e-4  # complex part of the refractive index (used to calculate mu)
        self.energy_eV = energy_ev  # energy of radiation in eV (8390 = Tungsten L alpha)

        # calculate remaining params
        self.lmbda = const.Planck * const.speed_of_light / (self.energy_eV * const.eV)  # x-ray wavelength in m
        self.mu = 4 * math.pi / self.lmbda * beta                                       # absorption coefficient in 1/m (mu = 4*pi/lmda*beta)
        self.delta_over_mu = delta / self.mu                                            # delta/mu ratio in m
        self.alpha = self.lmbda / (2 * math.pi) / self.delta_over_mu                    # ratio between phase shift and absorption (dimensionless). (alpha = 2*beta/delta = lmda/(2pi)/DeltaOverMu)


    def calculate_frequency_magnitude(self, image):
        """
        Calculate squared frequency magnitude matrix w2.
        :param image:   Image to perform phase-retrieval on as 2D numpy array. Only the shape is used here.
        """
        # pixels in x and y
        nx = image.shape[1]
        ny = image.shape[0]

        # frequencies in x and y
        u = np.linspace(-math.floor(nx / 2), math.floor((nx - 1) / 2), nx) / (nx * self.effective_pixelsize_x)
        v = np.linspace(-math.floor(ny / 2), math.floor((ny - 1) / 2), ny) / (ny * self.effective_pixelsize_y)

        # frequency matrices
        [U, V] = np.meshgrid(u, v)

        # frequency magnitude squared
        self.w2 = U ** 2 + V ** 2


    def print_params(self):
        """
        Print set and calculated params.
        """
        print("PHASE RETRIEVAL PARAMS:")
        print("\tPropagation distance\t= {} m".format(self.propagation_distance))
        print("\tPhoton Energy\t\t\t= {} eV".format(self.energy_eV))
        print("\tDelta\t\t\t\t\t= {}".format(self.delta))
        print("\tBeta\t\t\t\t\t= {}".format(self.beta))
        print("\tµ\t\t\t\t\t\t= {} 1/m".format(self.mu))
        print("\tDelta/µ\t\t\t\t\t= {} m".format(self.delta_over_mu))
        print("\tWavelength (λ)\t\t\t= {} m".format(self.lmbda))
        print("\tAlpha\t\t\t\t\t= {}".format(self.alpha))


    def paganin(self, img):
        """
        Apply Paganin phase-retrieval to a provided image.
        :param img:     Image in real space as 2D numpy array.
        :return:        2 Images as 2D numpy arrays. First image: filtered, Second image: phase.
        """
        h = 2 * math.pi * self.lmbda * self.propagation_distance * self.w2 + self.alpha
        filtered = self.__deconvolution__(img - 1, h)
        phase = -np.log(self.alpha * filtered + 1) / self.alpha
        return filtered, phase


    def convert_to_thickness(self, img, lmbda, delta):
        """
        Convert filtered or phase image to thickness image. Usual post-processing step for Paganin images.
        :param img:         Images to process as 2D numpy array.
        :param lmbd:        Lambda parameter from phase-retrieval.
        :param delta:       Delta parameter from phase-retrieval.
        :return:            Thickness image as 2D array, same shape as input image.
        """
        return lmbda * img / (2 * math.pi * delta)
