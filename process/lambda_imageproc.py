#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 18 20:36:00 2020

@brief:  Image processing methods for lambda data.

@author: Till, till.dreier@med.lu.se
"""

import os
import tqdm
import numpy as np
from PIL import Image
from scipy import interpolate
import matplotlib.pyplot as plt
from scipy.signal import convolve2d
from scipy.ndimage.filters import maximum_filter, median_filter
from skimage.feature import register_translation



class FilterOverUnderResponsive:
    """ USE IMAGEFILTER INSTEAD! """

    def __init__(self):
        """
        Find over- and under-responsive pixels using their neighbours.
        This class is not tested. Only tested this before moving into a class.
        remove_outliers() from ImageFilter does the same on over-responsive pixels, but faster.
        remove_dead_pixels() from ImageFilter does the same on under-responsive pixels, but faster.
        """
        self.condition_horizontal = None
        self.condition_vertical = None
        self.condition_diagonal = None
        self.margin_high = None
        self.margin_low = None
        self.mask_low = None
        self.mask_high = None


    @staticmethod
    def load_image(path, file):
        """ Load an image from path """
        image = np.array(Image.open(os.path.join(path, file)))
        return image


    @staticmethod
    def load_images_from_folder(path, extension=".tiff"):
        """ Load all images with extension from path """
        # get files with ending in path
        files = [f for f in os.listdir(path) if f.endswith(extension)]
        # get amount of files and image shape
        num_files = len(files)
        imgshape = Image.open(os.path.join(path, files[0])).size
        # allocate space
        arr = np.zeros((num_files, imgshape[0], imgshape[1]))
        # load all images
        for idx, file in enumerate(files):
            arr[idx] = np.array(Image.open(os.path.join(path, file)))
        return arr


    def set_condition_horizontal(self, condition=[1,1,0,1,1]):
        """ Set a condition (footprint) -> how many pixels to check in either direction in horizontal direction """
        self.condition_horizontal = np.asarray([condition])


    def reset_condition_horizontal(self):
        """ """
        self.condition_horizontal = None


    def set_condition_vertical(self, condition=[1,1,0,1,1]):
        """ Set a condition (footprint) -> how many pixels to check in either direction in vertical direction """
        self.condition_horizontal = np.asarray([condition]).T


    def reset_condition_vertical(self):
        """ """
        self.condition_vertical = None


    def set_condition_diagonal(self, condition=[[1,0,0],[0,0,0],[0,0,1]]):
        """ Set a condition (footprint) -> how many pixels to check diagonally """
        self.condition_diagonal = np.asarray([condition])


    def reset_condition_diagonal(self):
        """ """
        self.condition_diagonal = None


    def set_magin_high(self, margin):
        """ Set margin by which pixels have to be higher to be counted as over-responsive. """
        self.margin_high = margin


    def set_magin_low(self, margin):
        """ Set margin by which pixels have to be lower to be counted as under-responsive. """
        self.margin_low = margin


    def create_mask_high(self, image, mode='high'):
        """ """
        if mode == 'high':
            tmp = []
            for condition in filter(None, [self.condition_horizontal, self.condition_vertical, self.condition_diagonal]):
                tmp.append(image > maximum_filter(image, footprint=condition, mode='constant', cval=-np.inf) + self.margin_high)
            for idx, arr in enumerate(tmp):
                if idx == 0:
                    self.mask_high = arr
                else:
                    self.mask_high = np.logical_and(self.mask_high, arr)

        elif mode == 'low':
            tmp = []
            for condition in filter(None, [self.condition_horizontal, self.condition_vertical, self.condition_diagonal]):
                tmp.append(image < maximum_filter(image, footprint=condition, mode='constant', cval=-np.inf) - self.margin_low)
            for idx, arr in enumerate(tmp):
                if idx == 0:
                    self.mask_high = arr
                else:
                    self.mask_high = np.logical_and(self.mask_high, arr)

        else:
            print('[ERROR:] cannot create mask_detector, invalid mode. Valid: <high>, <low>.')


    def replace_masked_by_mean(self, image, verbose=False):
        """ Replace masked values with the global mean value """
        mean = np.mean(image)
        if self.mask_high is not None:
            if verbose: print('[DEBUG:] removing masked high values')
            image[self.mask_high] = mean
        if self.mask_low is not None:
            if verbose: print('[DEBUG:] removing masked low values')
            image[self.mask_low] = mean

        return image


    def replace_masked_by_localmean(self, image, kernel=[[0,1,0],[1,0,1],[0,1,0]], verbose=False):
        """ Replace masked values by the mean of their neighbours"""
        if self.mask_low is not None and self.mask_high is not None:
            combined_mask = self.mask_low | self.mask_high
            if verbose: print('[DEBUG:] using low and high mask_detector')
        elif self.mask_low is not None and self.mask_high is None:
            combined_mask = self.mask_low
            if verbose: print('[DEBUG:] using only low mask_detector')
        elif self.mask_low is None and self.mask_high is not None:
            combined_mask = self.mask_high
            if verbose: print('[DEBUG:] using only high mask_detector')
        else:
            combined_mask = np.zeros(image, dtype=bool)
            print('[CRITICAL:] no masks available!')

        kernel = np.array(kernel)
        conv_out = convolve2d(image, kernel, boundary='wrap', mode='same') / kernel.sum()
        filtered = image.copy()
        filtered[combined_mask] = conv_out[combined_mask]
        return filtered





class ImageRegister:
    """ """

    def __init__(self):
        """
        Register image shifts using cross-correlation.
        """
        self.roi_x_start = None
        self.roi_y_start = None
        self.roi_x_size = None
        self.roi_y_size = None

        self.x_shifts = None
        self.y_shifts = None


    def set_roi_image_registration(self, x_start, y_start, x_size, y_size):
        """
        Set the ROI parameters for image registration. These parameters will be used in the interpolate_modalities()
        function, which will select either the full image or a ROI to register shifts.
        :param x_start:     ROI start coordinate in X direction (horizontal).
        :param y_start:     ROI start coordinate in Y direction (vertical, 0 is at the top!).
        :param x_size:      Size of the ROI in X direction.
        :param y_size:      Size of the ROI in Y direction.
        """
        self.roi_x_start = x_start
        self.roi_y_start = y_start
        self.roi_x_size = x_size
        self.roi_y_size = y_size


    def reset_roi_image_registration(self):
        """
        Set the ROI parameters for image registration back to None.
        """
        self.roi_x_start = None
        self.roi_y_start = None
        self.roi_x_size = None
        self.roi_y_size = None


    @staticmethod
    def __register_images_roi__(images, upfac=100, roi_x_start=0, roi_y_start=0, roi_x_size=128, roi_y_size=128, debug=False):
        """
        Register a stack of images using only a defined region of interest.
        Images are registered to the previous image (should be a little more robust than always comparing to the first image).
        Shifts are corrected to refer to the first image.
        :param images:          Stack of images as numpy array of shape (img_num, x, y).
        :param upfac:           Upsampling factor used by registration algorithm, default 100.
        :param roi_x_start:     ROI start point in x direction.
        :param roi_y_start:     ROI start point in y direction.
        :param roi_x_size:      Size of ROI in x direction.
        :param roi_y_size:      Size of ROI in y direction.
        :param debug:           Plot the ROI when True. Default False.
        :return:                2 lists containing the shifts in x and y.
        """
        x_shifts, y_shifts = [], []
        roi_y_stop = roi_y_start + roi_y_size
        roi_x_stop = roi_x_start + roi_x_size

        if debug:
            plt.imshow(images[0, roi_y_start:roi_y_stop, roi_x_start:roi_x_stop])
            plt.title('[DEBUG:] ROI of reference image')
            plt.xlabel("X [pixel]")
            plt.ylabel("Y [pixel]")
            plt.show()

        # Comparing each image to the previous + considering that the data is arranged on a square
        # NOTE: this assume that the image positions 'turn_around', i.e. when at the end of a line, move down and then backwards.
        for idx in tqdm.tqdm(range(images.shape[0])):

            # compare to previous image
            if idx == 0:
                x_shifts.append(0)
                y_shifts.append(0)
            else:
                [y, x], _, _ = register_translation(images[idx - 1, roi_y_start:roi_y_stop, roi_x_start:roi_x_stop],
                                                    images[idx + 0, roi_y_start:roi_y_stop, roi_x_start:roi_x_stop],
                                                    upfac)
                y_shifts.append(y + y_shifts[-1])
                x_shifts.append(x + x_shifts[-1])

        return x_shifts, y_shifts


    @staticmethod
    def __register_images__(images, ref_img_id=0, ref_image=None, upfac=100):
        """
        Register the shifts between a stack of images (self.images) using cross-correlation.
        [1] Manuel Guizar-Sicairos, et al, "Efficient subpixel image registration algorithms," Opt. Lett. 33, 156-158 (2008).
        :param ref_img_id:  Index of the reference image in the stack. Default: 0, i.e. the first image.
        :param ref_image:   Provide a seperate reference image if the reference is not contained inside the stack.
                            Default: None, i.e. uses an image from IMAGES as reference.
        :param upfac:       Upsampling factor used by the algorithm.
        :return:            2 list containing the shifts in (x, y) in pixels.
        """
        x_shifts, y_shifts = [], []
        if ref_image is None:
            ref_image = images[ref_img_id]
        for image in tqdm.tqdm(images):
            [ys, xs], _, _ = register_translation(ref_image, image, upfac)
            x_shifts.append(xs)
            y_shifts.append(ys)
        return x_shifts, y_shifts


    def register_images(self, images, ref_idx=0, upfac=500, plot=True, plot_annotate=False):
        """
        Register provided images.
        Either using all images or using a ROI if previously defined via set_roi_image_registration().
        :param images:          Images to register as 3D numpy array of shape (img_num, x, y)
        :param ref_idx:         Index of the reference image for registration, default 0. Only used when no ROI defined.
        :param upfac:           Upsampling factor used be cross-correlation. Default 500.
        :param plot:            When True (default) the calculated shifts are plotted.
        :param plot_annotate:   Write annotations into debug plot.
        """
        if None in [self.roi_x_start, self.roi_y_start, self.roi_x_size, self.roi_y_size]:
            print('[DEBUG:] no or not all ROI parameters set, using full image for shift registration.')
            self.x_shifts, self.y_shifts = self.__register_images__(images, ref_idx, upfac=upfac)
        else:
            print('[DEBUG:] ROI parameters set, using ROI for shift registration.')
            self.x_shifts, self.y_shifts = self.__register_images_roi__(images, upfac=upfac,
                                                                        roi_x_start=self.roi_x_start,
                                                                        roi_y_start=self.roi_y_start,
                                                                        roi_x_size=self.roi_x_size,
                                                                        roi_y_size=self.roi_y_size)

        if plot is True:
            self.plot_shifts(plot_annotate)


    def plot_shifts(self, annotate=False):
        """
        Plot the shifts in x and y.
        """
        plt.plot(self.x_shifts, self.y_shifts, 'ro--')
        if annotate is True:
            for ix, iy in zip(self.x_shifts, self.y_shifts):
                plt.text(ix, iy, '({:.2f},{:.2f})'.format(ix, iy))
        plt.title("Calculated shifts")
        plt.xlabel("X shifts [pixel]")
        plt.ylabel("Y shifts [pixel]")
        plt.grid()
        plt.show()




class ImageInterpolate:
    """ """

    def __init__(self, interpolation_factor=None, interpolation_method=None, xshifts=None, yshifts=None):
        """
        Class to handle interpolation of shifted low-res images onto a high-res grid and combining those images
        to obtain a super-resolution image.
        :param interpolation_factor:    Interpolation factor (int). Set to sqrt(num_images).
        :param interpolation_method:    Interpolation method (string). Can be linear, cubic, quintic, or spline.
        :param xshifts:                 Shifts of the low-res images in x direction (list of floats).
        :param yshifts:                 Shifts of the low-res images in y direction (list of floats).
        """
        self.interpolation_factor = None
        self.interpolation_method = None
        self.x_shifts = xshifts
        self.y_shifts = yshifts
        self.interpolated_images = None
        self.srimage = None

        if interpolation_factor is not None: self.set_interpolation_factor(interpolation_factor)
        if interpolation_method is not None: self.set_interpolation_method(interpolation_method)


    def set_interpolation_factor(self, ifactor):
        """
        Set the interpolation factor.
        :param ifactor:     Interpolation factor (integer)
        """
        if type(ifactor) is int:
            self.interpolation_factor = ifactor
        else:
            self.interpolation_factor = 1
            print('[CRITICAL:] interpolation factor is not an integer, using 1.')


    def set_interpolation_method(self, imethod):
        """
        Set the interpolation method.
        :param imethod:     Interpolation method (string).
        """
        if imethod in ["linear", "cubic", "quintic", "spline"]:
            self.interpolation_method = imethod
        else:
            self.interpolation_method = "spline"
            print('[WARNING:] invalid interpolation method, using spline. Valid: linear, cubic, quintic, spline.')


    def set_shifts(self, xshifts, yshifts):
        """
        Load image shifts in x and y direction.
        :param xshifts:     List of shifts in x direction (list of floats).
        :param yshifts:     List of shifts in y direction (list of floats).
        """
        self.x_shifts = xshifts
        self.y_shifts = yshifts


    def __interpolate_image__(self, images, save=None):
        """
        Interpolate provided images in self.images using self.interpolation method and self.interpolation_factor.
        Save the results in self.interpolated_images, which is a 3D array of shape (img_nr, x, y) and has
        to be created before calling this function.
        :param save:    Save the interpolated images into a folder called "Interpolated" when a path is set. Don't save when None (default).
        :return:        True when done. False when error.
        """
        if self.interpolated_images is None:
            print('[ERROR:] interpolated_images class var is not initialised.')
            return False
        if self.interpolation_factor is None:
            print('[ERROR:] interpolation factor not set. Call set_interpolation_factor() to set it.')
            return False
        print('[INFO:] interpolating {} images by factor {} using {}'.format(images.shape[0], self.interpolation_factor, self.interpolation_method))

        # get image size
        _, y_size, x_size = images.shape

        # make LR grid
        xlr = np.linspace(0, x_size-1, x_size)
        ylr = np.linspace(0, y_size-1, y_size)

        for idx, (image, shift_x, shift_y) in enumerate(zip(images, self.x_shifts, self.y_shifts)):
            # shifted HR grid
            xhr = np.arange(0, x_size, 1 / self.interpolation_factor) - shift_x
            yhr = np.arange(0, y_size, 1 / self.interpolation_factor) - shift_y

            # interpolate
            if self.interpolation_method in ["cubic", "linear", "quintic"]:
                interpolator = interpolate.interp2d(xlr, ylr, image, kind=self.interpolation_method)
                tmp = interpolator(xhr, yhr)
            elif self.interpolation_method == "spline":
                interpolator = interpolate.RectBivariateSpline(ylr, xlr, image)
                tmp = interpolator(yhr, xhr)
            else:
                # error case, invalid interpolation method
                print('[ERROR:] invalid interpolation method: {}.'.format(self.interpolation_method))
                return False

            # save
            if save is not False:
                self.save_to_tiff(tmp, os.path.join(save, "Interpolated"), "interp_{:04}.tiff".format(idx))

            self.interpolated_images[idx] = tmp
        print('[DEBUG:] interpolated ({}) {} images by factor {}. New size {}x{} pixels'.format(self.interpolation_method, self.interpolated_images.shape[0], self.interpolation_factor, self.interpolated_images.shape[2], self.interpolated_images.shape[1]))
        return True


    def __interpolate_on_highres_grid__(self, images, save_path=None):
        """
        Calculate the shifts in (x, y) of self.images, allocate space for interpolated images and call the
        interpolation function.
        :param images:      Images to interpolate as 3D numpy array of shape (num_imgs, x, y)
        :param save_path:   Path to write interpolated images into as tiff files. Has to be set when save_interpolated_images is True.
        """
        if self.x_shifts is None or self.y_shifts is None:
            print("[CRITICAL:] shifts have not been calculated but register_images is set to False. Run register_images() func first!")
            return False
        else:
            print("[INFO:] using pre-calculated image shifts.")

        # allocate space for interpolated images
        interpolated_shape = (images.shape[0], images.shape[1] * self.interpolation_factor, images.shape[2] * self.interpolation_factor)
        self.interpolated_images = np.zeros(interpolated_shape, dtype=np.float32)

        # interpolate
        save_interpolated = True if save_path is not None else False
        self.__interpolate_image__(images, save_interpolated)


    def create_super_resolution_projection(self, images, save_folder, save_name='projection_0000.tiff', to_uint16=True, global_min=None, global_max=None):
        """
        Create a super-resolution projection from the provided images.
        Shifts (x_shifts, y_shifts), interpolation factor, and interpolation method have to be set.
        Interpolates the provided images on a shifted high-res grid and creates a super-res images by averaging.
        When processing multiple projections, provide global_min and global_max to reduce flickering.
        :param images:          Low-res images (e.g. a projection) to interpolate as 3D numpy array of shape (num_images, x, y).
        :param save_folder:     Path to folder to save super-res image into.
        :param save_name:       Name of the file to write
        :param to_uint16:       When True (default) convert the image to uint16.
        :param global_min:      Minimum value of the resulting uint16 TIFF. When using multiple projections, use minimum of all images.
        :param global_max:      Maximum value of the resulting uint16 TIFF. When using multiple projections, use maximum of all images.
        :return:
        """
        # interpolate provided images using self.x_shifts and self.y_shifts (saves into self.interpolated_images)
        self.__interpolate_on_highres_grid__(images)
        # combine images
        self.srimage = np.divide(np.sum(self.interpolated_images, axis=0), self.interpolated_images.shape[0])
        # save to disk
        self.save_to_tiff(self.srimage, save_folder, save_name, to_uint16, global_min, global_max)


    def save_to_tiff(self, image, path, fname, to_uint16=True, global_min=None, global_max=None):
        """
        Save an image to TIFF.
        :param image:           Image to write to TIFF as 2D numpy array.
        :param path:            Path to write image to.
        :param fname:           File name, .tiff will be appended if missing.
        :param to_uint16:       Convert image to uint16 when True (default).
                                When processing multiple images, provide global_min and max to avoid flickering.
        :param global_min:      Minimum value when converting to uint16.
        :param global_max:      Maximum value when converting to uint16.
        :return:
        """
        if to_uint16: image = self.__image_to_uint16__(image, global_min, global_max)
        if not os.path.isdir(path): os.mkdir(path)
        if ".tiff" not in fname: fname += ".tiff"
        tmp = Image.fromarray(image)
        tmp.save(os.path.join(path, fname))


    def __image_to_uint16__(self, image, global_min=None, global_max=None):
        """
        Convert a provided image to uint16.
        :param image:       Image (numpy array)
        :param global_max:  Force global maximum
        :return:            Input image converted to 16-bit unsigned integer.
        """
        # global or local (current image) maximum
        if global_max is not None:
            img_max = global_max
        else:
            img_max = np.amax(image)

        # global or local (current image) minimum
        if global_min is not None:
            img_min = global_min
        else:
            img_min = np.amin(image)

        # handle negative values
        if img_min < 0:
            image = np.add(image, abs(img_min))

        return np.multiply(np.divide(image, img_max), 2 ** 16 - 1).astype(np.uint16)



class ImageFilter:
    """ """

    def __init__(self):
        """
        Methods to remove outliers, noisy, broken, etc. pixels from images.
        remove_outliers():              Identifying pixels above the standard deviation (times a threshold) on a blurred
                                        (median filter) image and replacing them with values from the filtered image.
                                        Method is adapted from HoloTomoToolbox from Tim Salditt's Group in Göttingen.
        remove_masked():                Use a provided boolean mask to remove masked pixels. Replacement values are
                                        obtained via a median filter.
        remove_pixels_above_value():    Replace pixels above a certain value. Similar to remove_outliers(), but allows to
                                        specify the exact value at which to consider a pixel noisy.
        remove_dead_pixels():           Replace pixels below a certain value.
        prepare_and_apply_mask():       Execute the above functions together. Create a mask containing outliers,
                                        noisy (above value), and dead (below value) pixels and replace them with values
                                        from a median filtered image. This is faster since the median filter is the most
                                        time consuming part of the functions.
                                        remove_masked() is excluded for now, since this is using a much larger median filter
                                        to remove masked areas, which is more time consuming. (Using this for 6x6 pixels)
        """


    def remove_outliers(self, image, size=5, thl=2, max_pixels=500, verbose=False):
        """
        Remove hot pixels from image. Filling in values via a median filter.
        :param image:           Input image as 2D numpy array. Will be converted to float32 if necessary.
        :param size:            Size of the kernel used for median filter.
        :param thl:             Threshold for outlier detection (thl * std_dev).
        :param max_pixels:      Do not replace any pixels when more than this amount would be replaced.
        :param verbose:         When True, print the number of masked pixels and the standard deviation.
        :return:                Corrected image as 2D numpy array.
        """
        # convert to float if necessary
        if image.dtype not in [float, np.float32, np.float64]:
            image = image.astype(np.float32)

        # remove NaN
        image[np.isnan(image)] = np.inf

        # filter image
        filt = median_filter(image, size, mode='wrap')

        # difference image
        diff = np.subtract(image, filt)
        diff[np.isnan(diff)] = np.inf  # remove NaNs that might occur

        # standard deviation (ignoring inf)
        std_grey = np.std(diff[~np.isinf(diff)])  # standard deviation

        # mask_detector hot pixels
        mask = abs(diff) > (thl * std_grey)
        masked = len(np.where(mask >= 1)[0])

        if verbose:
            print('STD: {}'.format(std_grey))
            print('Hot pixels: {}'.format(masked))

        # throw warning when too many pixels found
        if masked > max_pixels:
            print('[WARNING:] more than {} outliers found. Skipping ...'.format(masked))
            return image

        else:

            # replace masked pixels
            corr = image.copy()
            corr[mask] = filt[mask]
            return corr


    def remove_masked(self, image, filter_size=10, mask=None, verbose=False):
        """
        Remove masked pixels from image. Filling in values via a median filter.
        :param image:           Input image as 2D numpy array.
        :param filter_size:     Size of the kernel for the median filter (10 works well on XL pixels of Lambda detector).
        :param mask:            A boolean mask of same shape as image.
        :param verbose:         When True, print the number of masked pixels.
        :return:                Filtered image as 2D numpy array.
        """
        if image.dtype not in [float, np.float32, np.float64]:
            image = image.astype(np.float32)

        if mask is None:
            # mask = self.mask_detector
            print('ERROR: ImageFilter class has no mask implemented. Mask has to be provided!')

        if mask.dtype is not bool:
            mask = mask.astype(bool)
            if verbose:
                print('[DEBUG:] converted mask to bool')

        # remove NaN
        image[np.isnan(image)] = np.inf

        # apply median filter
        filt = median_filter(image, filter_size)

        # replace masked values with values from filtered image
        if verbose:
            print('[DEBUG:] Masked pixels: {}'.format(len(np.where(mask >= 1)[0])))
        corr = image.copy()
        corr[mask] = filt[mask]
        return corr


    def remove_pixels_above_value(self, image, value=1.3, filter_size=5, max_pixels=500, verbose=False):
        """
        Remove pixels from image above value. Clean up images after removing outliers and mask_detector.
        :param image:       Image to remove pixels from as 2D numpy array.
        :param value:       Value above which to mask_detector a pixel. 1.3 works well after proper flat-field correction.
        :param filter_size: Size of the kernel for the median filter.
        :param max_pixels:  Maxmimum amount of pixels to remove. Throw warning if more and don't process image.
        :param verbose:     When True print the amount of pixels being removed.
        :return:            Cleaned up image as 2D numpy array.
        """
        mask = image > value
        mask = mask.astype(bool)
        masked = len(np.where(mask == True)[0])

        # throw warning when too many pixels found
        if masked > max_pixels:
            print('[WARNING:] more than {} noisy pixels (value > {}). Skipping ...'.format(masked, value))
            return image

        filtered = self.remove_masked(image, filter_size, mask, verbose)
        return filtered


    def remove_dead_pixels(self, image, value=0.1, filter_size=5, max_pixels=500, verbose=False):
        """
        Remove pixels from image below value. Dead pixels are not necessarily always 0.
        :param image:           Image to remove pixels from as 2D numpy array.
        :param value:           Value below which to consider pixels dead. 0.2 works well after proper flat-field correction.
        :param filter_size:     Size of the kernel for the median filter.
        :param max_pixels:      Maxmimum amount of pixels to remove. Throw warning if more and don't process image.
        :param verbose:         When True print the amount of dead pixels being removed.
        :return:                Cleaned up image as 2D numpy array.
        """

        # remove NaN
        image[np.isnan(image)] = np.inf

        mask = image <= value
        mask = mask.astype(bool)
        masked = len(np.where(mask == True)[0])

        if verbose:
            print('[DEBUG:] {} dead pixels'.format(masked))

        if masked > max_pixels:
            print('[WARNING:] more than {} dead pixels (value <= {}). Skipping ...'.format(masked, value))
            return image

        # apply median filter
        filt = median_filter(image, filter_size)

        corr = image.copy()
        corr[mask] = filt[mask]
        return corr


    def prepare_and_apply_mask(self, image, outliers=True, noisy=True, dead=True, apply_mask=True, mask=None,
                               filter_size=5, outliers_thl=2, noisy_thl=1.3, dead_thl=0.2,
                               max_pixels=1000, verbose=False):
        """
        Prepare a mask containing outliers, noisy and dead pixels and filter them out.
        This is quicker than filtering these 3 steps individually, since most time is spent on median filtering.
        To only create the mask, set apply_mask to False.
        To only apply a provided mask, set outliers, noisy, dead to False, apply_mask to True, and provide mask.
        :param image:           Image to replace pixels in as 2D numpy array.
        :param outliers:        When True, mask outliers.
        :param noisy:           When True, mask noisy.
        :param dead:            When True, mask dead.
        :param apply_mask:      When True, use the mask to filter image.
        :param mask:            When provided (2D boolean array), use this mask instead of creating one.
        :param filter_size:     Size of the median filter used to fill in masked values.
        :param outliers_thl:    Threshold for outlier detection (thl * std(diff_img), where diff_img = image - medianfilter(image)).
        :param noisy_thl:       Threshold above which to consider a pixel noisy. 1.3 works well for flat-field corrected images.
        :param dead_thl:        Threshold below which to consider a pixel dead. 0.2 or 0.1 work well.
        :param max_pixels:      Maximum amount of pixels to mask for each step.
        :param verbose:         When True, print debug information.
        :return:                Filtered image and mask as 2D arrays.
        """
        if image.dtype not in [float, np.float32, np.float64]:
            print('[ERROR:] images are not of type float. Flat-field correction not applied?')

        if mask is None:
            filter_mask = np.zeros(image.shape).astype(bool)
        else:
            filter_mask = mask.astype(bool)

        # prepare filtered image
        image[np.isnan(image)] = np.inf
        filt = median_filter(image, filter_size, mode='wrap')

        # mask outliers/hot pixels
        if outliers is True:

            # difference image
            diff = np.subtract(image, filt)
            diff[np.isnan(diff)] = np.inf  # remove NaNs that might occur

            # standard deviation (ignoring inf)
            std_grey = np.std(diff[~np.isinf(diff)])  # standard deviation

            # mask_detector hot pixels
            mask = abs(diff) > (outliers_thl * std_grey)
            masked = len(np.where(mask >= 1)[0])

            if verbose:
                print('\t{} outliers in image'.format(masked))

            if masked > max_pixels:
                print('\tMore than {} outliers in image. Skipping ...'.format(masked))
            else:
                filter_mask |= mask

        # mask dead pixels
        if dead is True:
            mask = image <= dead_thl
            mask = mask.astype(bool)
            masked = len(np.where(mask == True)[0])

            if verbose:
                print('\t{} dead pixels in image'.format(masked))

            if masked > max_pixels:
                print('\tMore than {} dead pixels in image. Skipping ...'.format(masked))
            else:
                filter_mask |= mask

        # mask noisy pixels
        if noisy is True:
            mask = image > noisy_thl
            mask = mask.astype(bool)
            masked = len(np.where(mask == True)[0])

            if verbose:
                print('\t{} noisy pixels in image'.format(masked))

            if masked > max_pixels:
                print('\tMore than {} noisy pixels in image. Skipping ...'.format(masked))
            else:
                filter_mask |= mask

        # apply mask to images
        if apply_mask is True:
            corr = image.copy()
            corr[filter_mask] = filt[filter_mask]
            return corr, filter_mask

        else:
            return image, filter_mask
